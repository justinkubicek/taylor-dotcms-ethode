let mix = require('laravel-mix');

mix.webpackConfig({ devtool: "inline-source-map" });

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', './virtual/shared.taylor.edu/application/themes/taylor-modernization/js')
	.js('resources/assets/js/zenscroll-min.js', './virtual/shared.taylor.edu/application/themes/taylor-modernization/js')
   	.sass('resources/assets/sass/app.scss', './virtual/shared.taylor.edu/application/themes/taylor-modernization/css').sourceMaps();