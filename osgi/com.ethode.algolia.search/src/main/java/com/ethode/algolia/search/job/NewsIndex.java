package com.ethode.algolia.search.job;

import com.algolia.search.*;
import com.algolia.search.objects.tasks.sync.*;

import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.Logger;

import com.ethode.algolia.search.classes.News;
import com.ethode.algolia.search.classes.Page;
import com.ethode.algolia.search.key.AlgoliaImporterKey;
import com.ethode.algolia.search.utilities.ContentletUtility;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Justin Kubicek on 4/14/2018.
 */
public class NewsIndex
{
	public static Class<NewsIndex> clazz = NewsIndex.class;

	private static final String APPLICATIONID = AlgoliaImporterKey.APPLICATIONID;
	private static final String SEARCHONLYAPIKEY = AlgoliaImporterKey.SEARCHONLYAPIKEY;
	private static final String ADMINAPIKEY = AlgoliaImporterKey.ADMINAPIKEY;
	private static final String NEWSCONTENTTYPEIDENTIFIER = AlgoliaImporterKey.NEWSCONTENTTYPEIDENTIFIER;

	public static void create()
	{
		Logger.info(clazz, "Starting create indexes.");

		//https://www.algolia.com/doc/api-client/java/getting-started/
		Logger.info(clazz, "Setting up networking...");
		java.security.Security.setProperty("networkaddress.cache.ttl", "60");
		// Connect to Algolia
		Logger.info(clazz, "Setting up Algolia client...");
		APIClient algoliaclient = new ApacheAPIClientBuilder(APPLICATIONID, ADMINAPIKEY).build();
		Logger.info(clazz, "Create a temporary index...");
		Index<News> algoliaindex = algoliaclient.initIndex( "tmp_news_index", News.class );
		Logger.info(clazz, "Index Ready.");

		Logger.info(clazz, "Get all dotCMS News");
		List<Contentlet> dotCMSNews = ContentletUtility.filterPublished( ContentletUtility.listContentlets( NEWSCONTENTTYPEIDENTIFIER ) );
		Logger.info(clazz, "Build a list for Algolia");
		List<News> newsitems = createAnAlgoliaNewsList( dotCMSNews );
		Logger.info(clazz, "Try adding these to the index");

		try{
			TaskSingleIndex task = algoliaindex.addObjects(newsitems);
			// task.waitForCompletion();
			Logger.info(clazz, "News were added.");
		} catch( Exception e ){
			Logger.info(clazz, "AlgoliaException: " + e);
		}

		// Then move the new index into place
		// Rename temp_news_index to MyIndex (and overwrite it)
		try{
			algoliaindex.moveTo("news_pages");
			// task.waitForCompletion();
			Logger.info(clazz, "Moved the new index to news_pages");
		} catch( Exception e ){
			Logger.info(clazz, "Error moving the new index in place: " + e);
		}
		

	}

	public static List<News> createAnAlgoliaNewsList ( List<Contentlet> newsitems )
	{
		List<News> algoliaNewsList = new ArrayList<News>();
		String friendlyname = "";
		String seodescription = "";
		String uri = "";

		for ( Contentlet newsitem : newsitems )
		{
			try{
				if( newsitem.getStringProperty( "friendlyname" ) == null ){
					friendlyname = "";
				}else{
					friendlyname = newsitem.getStringProperty( "friendlyname" );
				}
			}catch (Exception e ){
				friendlyname = "";
			}
			try{
				if( newsitem.getStringProperty( "seodescription" ) == null ){
					seodescription = "";
				}else{
					seodescription = newsitem.getStringProperty( "seodescription" );
				}
			}catch (Exception e ){
				seodescription = "";
			}
			try{
				if( newsitem.getStringProperty( "urlTitle" ) == null ){
					uri = "";
				}else{
					uri = "/news/" + newsitem.getStringProperty( "urlTitle" );
				}
			}catch (Exception e ){
				uri = "";
			}
			if ( friendlyname != ""){

				algoliaNewsList.add( new News().setPageTitle( friendlyname )
				.setPage_Meta_Description( seodescription )
				.setCategory( "News" )
				.setPage_Uri( uri ) );

			}
			
			
		}

		return algoliaNewsList;
	}

}