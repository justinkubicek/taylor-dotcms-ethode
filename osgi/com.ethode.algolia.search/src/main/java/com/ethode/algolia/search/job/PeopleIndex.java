package com.ethode.algolia.search.job;

import com.algolia.search.*;
import com.algolia.search.objects.tasks.sync.*;

import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.Logger;

import com.ethode.algolia.search.classes.People;
import com.ethode.algolia.search.classes.Page;
import com.ethode.algolia.search.key.AlgoliaImporterKey;
import com.ethode.algolia.search.utilities.ContentletUtility;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Justin Kubicek on 4/14/2018.
 */
public class PeopleIndex
{
	public static Class<PeopleIndex> clazz = PeopleIndex.class;

	private static final String APPLICATIONID = AlgoliaImporterKey.APPLICATIONID;
	private static final String SEARCHONLYAPIKEY = AlgoliaImporterKey.SEARCHONLYAPIKEY;
	private static final String ADMINAPIKEY = AlgoliaImporterKey.ADMINAPIKEY;
	private static final String PEOPLECONTENTTYPEIDENTIFIER = AlgoliaImporterKey.PEOPLECONTENTTYPEIDENTIFIER;

	public static void create()
	{
		Logger.info(clazz, "Starting create indexes.");

		//https://www.algolia.com/doc/api-client/java/getting-started/
		Logger.info(clazz, "Setting up networking...");
		java.security.Security.setProperty("networkaddress.cache.ttl", "60");
		// Connect to Algolia
		Logger.info(clazz, "Setting up Algolia client...");
		APIClient algoliaclient = new ApacheAPIClientBuilder(APPLICATIONID, ADMINAPIKEY).build();
		Logger.info(clazz, "Create a temporary index...");
		Index<People> algoliaindex = algoliaclient.initIndex( "tmp_people_index", People.class );
		Logger.info(clazz, "Index Ready.");

		Logger.info(clazz, "Get all dotCMS People");
		List<Contentlet> dotCMSPeople = ContentletUtility.filterPublished( ContentletUtility.listContentlets( PEOPLECONTENTTYPEIDENTIFIER ) );
		Logger.info(clazz, "Build a list for Algolia");
		List<People> people = createAnAlgoliaPeopleList( dotCMSPeople );
		Logger.info(clazz, "Try adding these to the index");

		try{
			TaskSingleIndex task = algoliaindex.addObjects(people);
			// task.waitForCompletion();
			Logger.info(clazz, "People were added.");
		} catch( Exception e ){
			Logger.info(clazz, "AlgoliaException: " + e);
		}

		// Then move the new index into place
		// Rename tmp_people_index to MyIndex (and overwrite it)
		try{
			algoliaindex.moveTo("people_pages");
			// task.waitForCompletion();
			Logger.info(clazz, "Moved the new index to people_pages");
		} catch( Exception e ){
			Logger.info(clazz, "Error moving the new index in place: " + e);
		}
		

	}

	public static List<People> createAnAlgoliaPeopleList ( List<Contentlet> people )
	{
		List<People> algoliaPeopleList = new ArrayList<People>();
		String friendlyname = "";
		String seodescription = "";
		String type = "";
		String urltitle = "";
		String uri = "";

		for ( Contentlet person : people )
		{
			try{
				if( person.getStringProperty( "friendlyname" ) == null ){
					friendlyname = "";
				}else{
					friendlyname = person.getStringProperty( "friendlyname" );
				}
			}catch (Exception e ){
				friendlyname = "";
			}
			try{
				if( person.getStringProperty( "seodescription" ) == null ){
					seodescription = "";
				}else{
					seodescription = person.getStringProperty( "seodescription" );
				}
			}catch (Exception e ){
				seodescription = "";
			}
			try{
				if( person.getStringProperty( "type1" ) == null ){
					type = "";
				}else{
					type = person.getStringProperty( "type1" );
				}
			}catch (Exception e ){
				type = "";
			}
			try{
				if( person.getStringProperty( "urlTitle" ) == null ){
					urltitle = "";
				}else{
					urltitle = person.getStringProperty( "urlTitle" );
				}
			}catch (Exception e ){
				urltitle = "";
			}
			
			uri = "/employee/" + type + "/" + urltitle;
			
			if ( friendlyname != ""){

				algoliaPeopleList.add( new People().setPageTitle( friendlyname )
				.setPage_Meta_Description( seodescription )
				.setCategory( "People" )
				.setPage_Uri( uri ) );

			}
			
			
		}

		return algoliaPeopleList;
	}

}