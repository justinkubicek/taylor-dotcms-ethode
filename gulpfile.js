var gulp = require('gulp');
var watch = require('gulp-watch');
var webdav = require('gulp-webdav-sync');
var plugins = require('gulp-load-plugins');
var livereload = require('gulp-livereload');
var debug = require('gulp-debug');
var gulputil = require('gulp-util');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');

// gulp.task( 'send', function () {
//   var options = {
//       protocol: 'http:'
//     , auth: {
//           'user': 'ethode_local'
//         , 'pass': 'AX5sb4Un$kHymfvqkb'
//       }
//     , hostname: 'shared.taylor.edu'
//     , base: 'virtual'
//     , port: 80
//     , pathname: '/webdav/live/1/'
//     , log: 'info' // show status codes 
//     , logAuth: false // show credentials in urls 
//     , uselastmodified: 11000
//   }
//   return gulp.src( 'virtual/**' )
//     .pipe( webdav( options ) );
// } );

gulp.task('watch-send', function() {
  livereload.listen();
  gulp.watch('virtual/**', function (a) {
    var options = {
      protocol: 'http:'
    , auth: {
          'user': 'ethode_local'
        , 'pass': 'AX5sb4Un$kHymfvqkb'
      }
    , hostname: 'shared.taylor.edu'
    , base: 'virtual'
    , port: 80
    , pathname: '/webdav/live/1/'
    , log: 'info' // show status codes 
    , logAuth: false // show credentials in urls 
    , uselastmodified: 11000
  }
    gulp.src( a.path )
    .pipe( plumber() )
    .pipe( webdav ( options ) )
    .pipe( plumber.stop() )
  });
});

gulp.task( 'send-after-mix', function () {
  var options = {
      protocol: 'http:'
    , auth: {
          'user': 'ethode_local'
        , 'pass': 'AX5sb4Un$kHymfvqkb'
      }
    , hostname: 'shared.taylor.edu'
    , base: 'virtual'
    , port: 80
    , pathname: '/webdav/live/1/'
    , log: 'info' // show status codes 
    , logAuth: false // show credentials in urls 
    , uselastmodified: 11000
  }
  return gulp.src( ['./virtual/shared.taylor.edu/application/themes/taylor-modernization/css/**', './virtual/shared.taylor.edu/application/themes/taylor-modernization/js/**'] )
    .pipe( webdav( options ) );
} );