package com.ethode.algolia.search.osgi;

// import com.dotcms.repackage.org.apache.logging.log4j.LogManager;
// import com.dotcms.repackage.org.apache.logging.log4j.core.LoggerContext;
import com.dotcms.repackage.org.osgi.framework.BundleContext;

// import com.dotmarketing.loggers.Log4jUtil;
import com.dotmarketing.osgi.GenericBundleActivator;
import com.dotmarketing.util.Logger;
import com.algolia.search.*;

import com.ethode.algolia.search.job.CreateIndexes;
import com.algolia.search.ApacheAPIClientBuilder;

/**
 * Created by Justin Kubicek on 2/18/2018.
 * Adpoted from code written by Nathan Keiter (nkeiter) on 12/13/2017.
 */
public class Activator extends GenericBundleActivator
{
	public static Class<Activator> clazz = Activator.class;

	// private LoggerContext pluginLoggerContext;

	private static final String PLUGIN_NAME = "dotCMS Algolia Search Plugin";

	@Override
	public void start( BundleContext bundleContext ) throws Exception
	{
		// Initializing log4j...
		// LoggerContext dotcmsLoggerContext = Log4jUtil.getLoggerContext();

		// Initialing the log4j context of this plugin based on the dotCMS logger context
		// this.pluginLoggerContext = (LoggerContext) LogManager.getContext( clazz.getClassLoader(), false, dotcmsLoggerContext, dotcmsLoggerContext.getConfigLocation() );

		// Initializing services...
		initializeServices( bundleContext );

		Logger.info( this, "+++++++++++++++++++++++++++++++++++++++++++++++" );
		Logger.info( this, PLUGIN_NAME + " Plugin Has Loaded" );
		Logger.info( this, "+++++++++++++++++++++++++++++++++++++++++++++++" );
		// Create Indexes
		CreateIndexes.createIndexes();
	}

	@Override
	public void stop( BundleContext bundleContext ) throws Exception
	{
		Logger.info( this, "+++++++++++++++++++++++++++++++++++++++++++++++" );
		Logger.info( this,  PLUGIN_NAME + " Plugin Has Stopped" );
		Logger.info( this, "+++++++++++++++++++++++++++++++++++++++++++++++" );

        //Unregister all the bundle services
        unregisterServices( bundleContext );

		// Shutting down log4j in order to avoid memory leaks
		// Log4jUtil.shutdown( pluginLoggerContext );
	}
}