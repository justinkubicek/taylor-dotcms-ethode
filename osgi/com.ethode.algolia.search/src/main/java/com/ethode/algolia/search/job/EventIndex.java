package com.ethode.algolia.search.job;

import com.algolia.search.*;
import com.algolia.search.objects.tasks.sync.*;

import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.Logger;

import com.ethode.algolia.search.classes.Event;
import com.ethode.algolia.search.classes.Page;
import com.ethode.algolia.search.key.AlgoliaImporterKey;
import com.ethode.algolia.search.utilities.ContentletUtility;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Justin Kubicek on 4/14/2018.
 */
public class EventIndex
{
	public static Class<EventIndex> clazz = EventIndex.class;

	private static final String APPLICATIONID = AlgoliaImporterKey.APPLICATIONID;
	private static final String SEARCHONLYAPIKEY = AlgoliaImporterKey.SEARCHONLYAPIKEY;
	private static final String ADMINAPIKEY = AlgoliaImporterKey.ADMINAPIKEY;
	private static final String EVENTCONTENTTYPEIDENTIFIER = AlgoliaImporterKey.EVENTCONTENTTYPEIDENTIFIER;

	public static void create()
	{
		Logger.info(clazz, "Starting create indexes.");

		//https://www.algolia.com/doc/api-client/java/getting-started/
		Logger.info(clazz, "Setting up networking...");
		java.security.Security.setProperty("networkaddress.cache.ttl", "60");
		// Connect to Algolia
		Logger.info(clazz, "Setting up Algolia client...");
		APIClient algoliaclient = new ApacheAPIClientBuilder(APPLICATIONID, ADMINAPIKEY).build();
		Logger.info(clazz, "Create a temporary index...");
		Index<Event> algoliaindex = algoliaclient.initIndex( "tmp_event_index", Event.class );
		Logger.info(clazz, "Index Ready.");

		Logger.info(clazz, "Get all dotCMS Events");
		List<Contentlet> dotCMSEvents = ContentletUtility.filterPublished( ContentletUtility.listContentlets( EVENTCONTENTTYPEIDENTIFIER ) );
		Logger.info(clazz, "Build a list for Algolia");
		List<Event> events = createAnAlgoliaEventList( dotCMSEvents );
		Logger.info(clazz, "Try adding these to the index");

		try{
			TaskSingleIndex task = algoliaindex.addObjects(events);
			// task.waitForCompletion();
			Logger.info(clazz, "Events were added.");
		} catch( Exception e ){
			Logger.info(clazz, "AlgoliaException: " + e);
		}

		// Then move the new index into place
		// Rename dev_event_index to MyIndex (and overwrite it)
		try{
			algoliaindex.moveTo("event_pages");
			// task.waitForCompletion();
			Logger.info(clazz, "Moved the new index to event_pages");
		} catch( Exception e ){
			Logger.info(clazz, "Error moving the new index in place: " + e);
		}
		

	}

	public static List<Event> createAnAlgoliaEventList ( List<Contentlet> events )
	{
		List<Event> algoliaEventList = new ArrayList<Event>();
		String friendlyname = "";
		String seodescription = "";
		String uri = "";

		for ( Contentlet event : events )
		{
			try{
				if( event.getStringProperty( "friendlyname" ) == null ){
					friendlyname = "";
				}else{
					friendlyname = event.getStringProperty( "friendlyname" );
				}
			}catch (Exception e ){
				friendlyname = "";
			}
			try{
				if( event.getStringProperty( "seodescription" ) == null ){
					seodescription = "";
				}else{
					seodescription = event.getStringProperty( "seodescription" );
				}
			}catch (Exception e ){
				seodescription = "";
			}
			try{
				if( event.getStringProperty( "urlTitle" ) == null ){
					uri = "";
				}else{
					uri = "/calendar-events/" + event.getStringProperty( "urlTitle" );
				}
			}catch (Exception e ){
				uri = "";
			}
			if ( friendlyname != ""){

				algoliaEventList.add( new Event().setPageTitle( friendlyname )
				.setPage_Meta_Description( seodescription )
				.setCategory( "Events" )
				.setPage_Uri( uri ) );

			}
			
			
		}

		return algoliaEventList;
	}

}