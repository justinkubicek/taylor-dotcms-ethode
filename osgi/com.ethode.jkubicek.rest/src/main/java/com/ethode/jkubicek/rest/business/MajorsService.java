package com.ethode.jkubicek.rest.business;

import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.UtilMethods;

import com.ethode.jkubicek.rest.beans.RestOptions;
import com.ethode.jkubicek.rest.log.Logger;
import com.ethode.jkubicek.rest.utilities.ContentletService;
import com.ethode.jkubicek.rest.utilities.ContentletUtility;

import com.dotmarketing.util.json.JSONArray;
import com.dotmarketing.util.json.JSONException;
import com.dotmarketing.util.json.JSONObject;

import com.ethode.jkubicek.rest.utilities.JSONUtility;

import java.util.ArrayList;
import java.util.List;

public class MajorsService
{
	public static Class<MajorsService> clazz = MajorsService.class;

	public static JSONArray getAffinityGroups( RestOptions restOptions, JSONArray affinityGroups  ){
		Contentlet contentlet;
		List<Contentlet> listOfPreMedMajors = null;
		List<Contentlet> listOfMajors = null;
		List<Contentlet> merge = null;
		JSONArray listOfRelatedMajors;
		JSONArray jsonArrayOfPreMedMajors;
		ArrayList<JSONArray> listToJoin = new ArrayList<JSONArray>();
		JSONObject jsonObject;
		JSONObject preMedObject;
		try{
			Logger.info( clazz, "Got to getAffinityGroups( restOptions, affinityGroups )" );
			//Get a list of all afinity groups
			affinityGroups = JSONUtility.contentletsToJSONArray( ContentletUtility.listContentlets("8b66c4be-139a-4f36-9686-85807100d073") );

			for(int i = 0;i < affinityGroups.length(); i++){
				jsonObject = affinityGroups.getJSONObject(i);
				contentlet = ContentletUtility.findContentletByIdentifier( jsonObject.getString("identifier") );
				listToJoin = new ArrayList<JSONArray>();
				listOfRelatedMajors = new JSONArray();
				if (contentlet != null){
					listOfPreMedMajors = ContentletService.getRelatedContent(contentlet, "AffinityGroup", "PreMedDegreesMajorsAndMinors");
					if(listOfPreMedMajors != null){
						//Add the words (Pre-Med to the end of the title for PreMed Items)
						jsonArrayOfPreMedMajors = JSONUtility.contentletsToJSONArray( listOfPreMedMajors );
						for(int x=0;x<jsonArrayOfPreMedMajors.length();x++){
							preMedObject = jsonArrayOfPreMedMajors.getJSONObject(x);
							preMedObject.put("title", preMedObject.getString("title") + " (Pre-Med)");
						}
						listToJoin.add( jsonArrayOfPreMedMajors );
					}
					listOfMajors = ContentletService.getRelatedContent(contentlet, "AffinityGroup", "DegreesMajorsAndMinors");
					if(listOfMajors != null){
					listToJoin.add( JSONUtility.contentletsToJSONArray( listOfMajors ) );
					}
					listOfRelatedMajors = getMergeJsonArrays( listToJoin );
				}
				jsonObject.put("relatedmajors", listOfRelatedMajors);
			}

		}
		catch( Exception exception )
		{
			Logger.error( clazz, "getAffinityGroups( restOptions, affinityGroups )", exception );
		}

		return affinityGroups;
	}

	public static JSONArray getMergeJsonArrays(ArrayList<JSONArray> jsonArrays) throws JSONException
	{
		JSONArray MergedJsonArrays= new JSONArray();
		 for(JSONArray tmpArray:jsonArrays)
		 {
			for(int i=0;i<tmpArray.length();i++)
			{
				MergedJsonArrays.put(tmpArray.get(i));
			}
		 }
		return MergedJsonArrays;
	}
}
