package com.ethode.algolia.search.job;

import com.algolia.search.*;
import com.algolia.search.objects.tasks.sync.*;

import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.Logger;

import com.ethode.algolia.search.classes.Course;
import com.ethode.algolia.search.classes.Page;
import com.ethode.algolia.search.key.AlgoliaImporterKey;
import com.ethode.algolia.search.utilities.ContentletUtility;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Justin Kubicek on 2/25/2018.
 */
public class CourseIndex
{
	public static Class<CourseIndex> clazz = CourseIndex.class;

	private static final String APPLICATIONID = AlgoliaImporterKey.APPLICATIONID;
	private static final String SEARCHONLYAPIKEY = AlgoliaImporterKey.SEARCHONLYAPIKEY;
	private static final String ADMINAPIKEY = AlgoliaImporterKey.ADMINAPIKEY;
	private static final String COURSECONTENTTYPEIDENTIFIER = AlgoliaImporterKey.COURSECONTENTTYPEIDENTIFIER;

	public static void create()
	{
		Logger.info(clazz, "Starting create indexes.");

		//https://www.algolia.com/doc/api-client/java/getting-started/
		Logger.info(clazz, "Setting up networking...");
		java.security.Security.setProperty("networkaddress.cache.ttl", "60");
		// Connect to Algolia
		Logger.info(clazz, "Setting up Algolia client...");
		APIClient algoliaclient = new ApacheAPIClientBuilder(APPLICATIONID, ADMINAPIKEY).build();
		Logger.info(clazz, "Create a temporary index...");
		Index<Course> algoliaindex = algoliaclient.initIndex( "tmp_course_index", Course.class );
		Logger.info(clazz, "Index Ready.");

		Logger.info(clazz, "Get all dotCMS DegreesMajorsAndMinors");
		List<Contentlet> dotCMSCourses = ContentletUtility.filterPublished( ContentletUtility.listContentlets( COURSECONTENTTYPEIDENTIFIER ) );
		Logger.info(clazz, "Build a list for Algolia");
		List<Course> courses = createAnAlgoliaCourseList( dotCMSCourses );
		Logger.info(clazz, "Try adding these to the index");

		try{
			TaskSingleIndex task = algoliaindex.addObjects(courses);
			// task.waitForCompletion();
			Logger.info(clazz, "Courses were added.");
		} catch( Exception e ){
			Logger.info(clazz, "AlgoliaException: " + e);
		}

		// Then move the new index into place
		// Rename dev_course_index to MyIndex (and overwrite it)
		try{
			algoliaindex.moveTo("studies_pages");
			// task.waitForCompletion();
			Logger.info(clazz, "Moved the new index to studies_pages");
		} catch( Exception e ){
			Logger.info(clazz, "Error moving the new index in place: " + e);
		}
		

	}

	public static List<Course> createAnAlgoliaCourseList ( List<Contentlet> courses )
	{
		List<Course> algoliaCourseList = new ArrayList<Course>();
		Boolean modernized = false;
		String friendlyname = "";
		String seodescription = "";
		String uri = "";

		for ( Contentlet course : courses )
		{
			try{
				if( course.getBoolProperty("modernized") ){
					modernized = true;
				}else{
					modernized = false;
				}
			}catch (Exception e ){
				modernized = false;
			}
			try{
				if( course.getStringProperty( "friendlyname" ) == null ){
					friendlyname = "";
				}else{
					friendlyname = course.getStringProperty( "friendlyname" );
				}
			}catch (Exception e ){
				friendlyname = "";
			}
			try{
				if( course.getStringProperty( "seodescription" ) == null ){
					seodescription = "";
				}else{
					seodescription = course.getStringProperty( "seodescription" );
				}
			}catch (Exception e ){
				seodescription = "";
			}
			try{
				if( course.getStringProperty( "uniqueid" ) == null ){
					uri = "";
				}else{
					uri = "/majors/" + course.getStringProperty( "uniqueid" );
				}
			}catch (Exception e ){
				uri = "";
			}
			if ( modernized && friendlyname != ""){

				algoliaCourseList.add( new Course().setPageTitle( friendlyname )
				.setPage_Meta_Description( seodescription )
				.setCategory( "Studies" )
				.setPage_Uri(  uri ) );

			}
			
			
		}

		return algoliaCourseList;
	}

}