package com.ethode.algolia.search.classes;

public class Publication {

    private String page_title;
    private String page_meta_description;
    private String category;
    private String page_uri;

  
    public Publication() {}

    //page_title
  
    public String getPageTitle() {
      return page_title;
    }
  
    public Publication setPageTitle( String page_title ) {
      this.page_title = page_title;
      return this;
    }

    //page_meta_description

    public String getPage_Meta_Description() {
      return page_meta_description;
    }
  
    public Publication setPage_Meta_Description( String page_meta_description ) {
      this.page_meta_description = page_meta_description;
      return this;
    }

    //category

    public String getCategory() {
      return category;
    }
  
    public Publication setCategory( String category ) {
      this.category = category;
      return this;
    }


    //page_uri

    public String getPage_Uri() {
      return page_uri;
    }

    public Publication setPage_Uri( String page_uri ) {
      this.page_uri = page_uri;
      return this;
    }

  }

  // Example JSON
  // [
  //   {
  //     "page_title": "Accounting",
  //     "page_meta_description": "Learn how to prepare and interpret financial data for managers, investors and the general public as you train in our accounting program.",
  //     "category": "Studies",
  //     "page_uri": "/accounting"
  //   },
  //   {
  //     "page_title": "Applied Missions",
  //     "page_meta_description": "Combine classroom study and a cross-cultural mission-oriented experience. Explore the history and present state of missions work in the world.",
  //     "category": "Studies",
  //     "page_uri": "/applied-missions"
  //   }
  // ]