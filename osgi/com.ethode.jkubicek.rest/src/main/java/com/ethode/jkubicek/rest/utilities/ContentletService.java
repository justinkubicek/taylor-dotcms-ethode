package com.ethode.jkubicek.rest.utilities;

import com.dotmarketing.business.APILocator;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;

import com.ethode.jkubicek.rest.log.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ContentletService {
    public static Class<ContentletService> clazz = ContentletService.class;

    public List<String> contentletCheck = new ArrayList<>();

    public static Contentlet setContentletStringField(Contentlet currentContentlet, String FiledName, String newString) {
        if (!newString.equals("false")) {
            currentContentlet.setStringProperty(FiledName, newString);
            return currentContentlet;
        } else {
            Logger.info(clazz, "Setting parenturltitle to nothing");
            currentContentlet.setStringProperty(FiledName, "");
            return currentContentlet;
        }
    }


    public static List<Contentlet> getRelatedContent(Contentlet contentlet, String parent, String child) {
        try {
            Map<Relationship, List<Contentlet>> relationships = APILocator.getContentletAPI()
                    .findContentRelationships(contentlet, APILocator.getUserAPI().getSystemUser());

            for (Map.Entry<Relationship, List<Contentlet>> rel : relationships.entrySet()) {
                if (rel.getKey().getParentRelationName().equalsIgnoreCase(parent) && rel.getKey().getChildRelationName()
                        .equalsIgnoreCase(child)) {
                    List<Contentlet> parentContentlets = APILocator.getContentletAPI().getRelatedContent(contentlet,
                            rel.getKey(), false, APILocator.getUserAPI().getSystemUser(), false);
                    //return filterPublished(rel.getValue());
                    return filterPublished(parentContentlets);
                }
            }
        } catch (DotDataException | DotSecurityException e) {
            return new ArrayList<>();
        }

        return new ArrayList<>();
    }

    public static List<Contentlet> filterPublished(List<Contentlet> contentlets) {
        List<Contentlet> filteredList = new ArrayList<>();
        for (Contentlet contentlet : contentlets) {
            try {
                if (contentlet.isLive()) {
                    filteredList.add(contentlet);
                }
            } catch (DotDataException | DotSecurityException e) {
                // Oh well!
            }
        }
        return filteredList;
    }


    public static Contentlet dummyFunction(Contentlet currentContentlet) {
        Logger.info(clazz, "Dummy Function Called for " + currentContentlet.getStructure()
                .getVelocityVarName());
        return currentContentlet;
    }


    public static boolean isContentletOfType(Contentlet currentContentlet, String velocityVarName) {
        return currentContentlet.getStructure().getVelocityVarName().equalsIgnoreCase(velocityVarName);
    }

}