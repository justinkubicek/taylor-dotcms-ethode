package com.ethode.algolia.search.job;

import com.algolia.search.*;
import com.algolia.search.objects.tasks.sync.*;

import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.beans.Identifier;
import com.dotmarketing.business.IdentifierAPI;
import com.dotmarketing.portlets.fileassets.business.FileAssetAPI;
import com.dotmarketing.business.APILocator;
import com.dotmarketing.util.Logger;

import com.ethode.algolia.search.classes.Course;
import com.ethode.algolia.search.classes.Page;
import com.ethode.algolia.search.key.AlgoliaImporterKey;
import com.ethode.algolia.search.utilities.ContentletUtility;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Justin Kubicek on 2/25/2018.
 */
public class PagesIndex
{
	public static Class<PagesIndex> clazz = PagesIndex.class;

	private static final String APPLICATIONID = AlgoliaImporterKey.APPLICATIONID;
	private static final String SEARCHONLYAPIKEY = AlgoliaImporterKey.SEARCHONLYAPIKEY;
	private static final String ADMINAPIKEY = AlgoliaImporterKey.ADMINAPIKEY;
	private static final String PAGECONTENTTYPEIDENTIFIER = AlgoliaImporterKey.PAGECONTENTTYPEIDENTIFIER;

	public static void create()
	{
		Logger.info(clazz, "Starting create indexes.");

		//https://www.algolia.com/doc/api-client/java/getting-started/
		Logger.info(clazz, "Setting up networking...");
		java.security.Security.setProperty("networkaddress.cache.ttl", "60");
		// Connect to Algolia
		Logger.info(clazz, "Setting up Algolia client...");
		APIClient algoliaclient = new ApacheAPIClientBuilder(APPLICATIONID, ADMINAPIKEY).build();
		Logger.info(clazz, "Create a temporary index...");
		Index<Page> algoliaindex = algoliaclient.initIndex( "tmp_pages_index", Page.class );
		Logger.info(clazz, "Index Ready.");

		Logger.info(clazz, "Get all dotCMS htmlpageassets");
		List<Contentlet> dotCMSPages = ContentletUtility.filterPublished( ContentletUtility.listContentlets( PAGECONTENTTYPEIDENTIFIER ) );
		Logger.info(clazz, "Build a list for Algolia");
		List<Page> pages = createAnAlgoliaPageList( dotCMSPages );
		Logger.info(clazz, "Try adding these to the index");

		try{
			TaskSingleIndex task = algoliaindex.addObjects(pages);
			// task.waitForCompletion();
			Logger.info(clazz, "Pages were added.");
		} catch( Exception e ){
			Logger.info(clazz, "AlgoliaException: " + e);
		}

		// Then move the new index into place
		// Rename dev_page_index to MyIndex (and overwrite it)
		try{
			algoliaindex.moveTo("other_pages");
			// task.waitForCompletion();
			Logger.info(clazz, "Moved the new index to other_pages");
		} catch( Exception e ){
			Logger.info(clazz, "Error moving the new index in place: " + e);
		}

	}

	public static List<Page> createAnAlgoliaPageList ( List<Contentlet> pages )
	{
		List<Page> algoliaPageList = new ArrayList<Page>();
		String friendlynameVar = "";
		String seodescriptionVar = "";
		String pageUrlVar = "";
		String showOnSitemap = "";
		Identifier identifier = new Identifier();

		for ( Contentlet page : pages )
		{
			try{
				if( page.getStringProperty( "friendlyname" ) == null ){
					friendlynameVar = "";
				}else{
					friendlynameVar = page.getStringProperty( "friendlyname" );
				}
			}catch (Exception e ){
				friendlynameVar = "";
			}
			try{
				if( page.getStringProperty( "seodescription" ) == null ){
					seodescriptionVar = "";
				}else{
					seodescriptionVar = page.getStringProperty( "seodescription" );
				}
			}catch (Exception e ){
				seodescriptionVar = "";
			}
			try{
				identifier = APILocator.getIdentifierAPI().find( page );
				pageUrlVar = identifier.getURI()
				.replace("/index.shtml", "/")
				.replace("/index", "/");
				if( pageUrlVar != null ){
					pageUrlVar = pageUrlVar;
				}else{
					pageUrlVar = "";
				}
			}catch (Exception e ){
				pageUrlVar = "";
			}
			try{
				if( page.getStringProperty( "showOnSitemap" ) == null ){
					showOnSitemap = "0";
				}else{
					showOnSitemap = page.getStringProperty( "showOnSitemap" );
				}
			}catch (Exception e ){
				showOnSitemap = "0";
			}

			if( friendlynameVar != "" && showOnSitemap.equals("1") ){

				algoliaPageList.add( new Page()
				.setPageTitle( friendlynameVar )
				.setPage_Meta_Description( seodescriptionVar )
				.setCategory( "Pages" )
				.setPage_Uri( pageUrlVar )
				);
				
			}
		}

		return algoliaPageList;
	}

}