package com.ethode.jkubicek.rest.utilities;

import com.dotmarketing.business.APILocator;
import com.dotmarketing.business.CacheLocator;
import com.dotmarketing.business.UserAPI;
import com.dotmarketing.cache.ContentTypeCache;
import com.dotmarketing.portlets.contentlet.business.ContentletAPI;
import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.UtilMethods;

import com.liferay.portal.model.User;

import com.ethode.jkubicek.rest.log.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContentletUtility
{
	
	public static final Class<ContentletUtility> clazz = ContentletUtility.class;

	// Takes in a type of contentlet, returns a list of all contentlets of that type
	public static List<Contentlet> listContentlets( String structureINode )
	{
		List<Contentlet> contentlets = null;
	
		try
		{
			// dotCMS API's
			ContentletAPI contentletAPI = APILocator.getContentletAPI();
			UserAPI userAPI = APILocator.getUserAPI();
			User systemUser = userAPI.getSystemUser();

			contentlets = contentletAPI.findByStructure( structureINode, systemUser, true, 0, 0 );
		}
		catch ( Exception exception )
		{
			Logger.error( clazz, "Unable to get all contentlets", exception );
		}

		return contentlets;
	}


	public static Contentlet findContentletByIdentifier( String identifier )
	{
		Contentlet foundContentlet = null;
		try
		{
			// dotCMS API's
			ContentletAPI contentletAPI = APILocator.getContentletAPI();
			UserAPI userAPI = APILocator.getUserAPI();
			User systemUser = userAPI.getSystemUser();

			foundContentlet = contentletAPI.findContentletByIdentifier( identifier, false, 1, systemUser, true);

		}
		catch ( Exception exception )
		{
			Logger.error( clazz, "findContentletByIdentifier( String identifier ): identifier" + identifier, exception );
		}

		return foundContentlet;
	}

}