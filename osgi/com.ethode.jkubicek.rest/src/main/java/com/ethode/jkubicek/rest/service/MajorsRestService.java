package com.ethode.jkubicek.rest.service;

import com.dotcms.repackage.javax.ws.rs.GET;
import com.dotcms.repackage.javax.ws.rs.Path;
import com.dotcms.repackage.javax.ws.rs.PathParam;
import com.dotcms.repackage.javax.ws.rs.core.Context;
import com.dotcms.repackage.javax.ws.rs.core.Response;

import com.dotcms.rest.InitDataObject;
import com.dotcms.rest.annotation.NoCache;

import com.dotmarketing.util.json.JSONArray;
import com.dotmarketing.util.json.JSONException;
import com.dotmarketing.util.json.JSONObject;

import com.ethode.jkubicek.rest.beans.RestOptions;
import com.ethode.jkubicek.rest.business.MajorsService;
import com.ethode.jkubicek.rest.log.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Pass an AffinityGroup. (Default return just a list of all AffinityGroups)
 * http://wwwpprd.taylor.edu.edu/ap/imajors/affinitygroups/
 * 
 */

@Path( "/majors" )
public class MajorsRestService extends BasicRestService
{
	public static Class<MajorsRestService> clazz = MajorsRestService.class;

	@NoCache
	@GET
	@Path( "/{parameters: .*}" )
	public Response getContent( @Context HttpServletRequest httpServletRequest, @Context HttpServletResponse httpServletResponse, @PathParam( "parameters" ) String parameters )
	{
		JSONObject jsonObject = new JSONObject();

		try
		{
			Logger.info( clazz, "Got to getContent( HttpServletRequest, HttpServletResponse, String )" );

			InitDataObject initDataObject = webResource.init( parameters, true, httpServletRequest, false, null );
			RestOptions restOptions = new RestOptions( initDataObject );
			JSONArray jsonArray = new JSONArray();

			jsonArray = MajorsService.getAffinityGroups( restOptions, jsonArray );

			jsonObject.put( "status", "success" );
			jsonObject.put( "message", "" );
			jsonObject.put( "results", jsonArray );

			return super.respond( jsonObject );
		}
		catch ( Exception exception )
		{
			try
			{
				jsonObject.put( "status", "error" );
				jsonObject.put( "message", exception );
				jsonObject.put( "results", "[]" );
			}
			catch ( JSONException jsonException )
			{
				Logger.error( this, "Error getting error.", jsonException );
			}

			Logger.error( this, "Error searching contentlets.", exception );

			return super.respond( jsonObject );
		}

	}
}