# Taylor.edu-Modernization-Project

This is the repo for the Taylor.edu Modernization Project website.

## Prerequisites
- [Git](https://git-scm.com/)
- [Github Desktop](https://desktop.github.com/) Optional
- [Node.js](https://nodejs.org/)/[npm](https://www.npmjs.com/) This is optional, as gradle will download NodeJS.
- Java 8 SDK: [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) or [OpenJDK](http://openjdk.java.net/install/)

## Getting Started Writing Code on www.taylor.edu
1. Install NPM packages:
    * `./gradlew npmInstall`
    * - or -
    * `npm Install`

2. Develop locally with reflection on the dev server
    * `gulp watch-send`
    * - or -
    * `./gradlew watch-send`

3. Modifying CSS/Sass/JS
    * Make your changes.
    * Compile.
    * `npm run prod`
    * Deploy.
    * `gulp send-after-mix`

#### Known Issues with gulp watch-send ####
While the command is running it will show an error when you, create, rename, or delete a file/directory.
* If you need to do one of these things, stop the script, preform the action, and save the file again so it uploads to the dotCMS server.
* Check the site browser for any old filenames, folders, or empty files.

#### Comparing the repo to code to the project ####
You can use the following command to find the differences between the master branch and the dev server before deploying the master branch to the dev server
    * `diff --brief -r ./virtual/shared.taylor.edu/application/ /Volumes/1/shared.taylor.edu/application/`

#### Plugins ####