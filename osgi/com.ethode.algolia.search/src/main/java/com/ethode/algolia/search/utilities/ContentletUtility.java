package com.ethode.algolia.search.utilities;

import com.dotmarketing.business.APILocator;
import com.dotmarketing.business.CacheLocator;
import com.dotmarketing.business.UserAPI;
import com.dotmarketing.cache.ContentTypeCache;
import com.dotmarketing.portlets.contentlet.business.ContentletAPI;
import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.Logger;
import com.dotmarketing.util.UtilMethods;

import com.liferay.portal.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Nathan Keiter (nkeiter) Modifed Justin Kubicek
 * Modified by Nathan Keiter (nkeiter) on 12/13/2017.
 */

public class ContentletUtility
{
	
	public static final Class<ContentletUtility> clazz = ContentletUtility.class;

	// Takes in a type of contentlet, returns a list of all contentlets of that type
	public static List<Contentlet> listContentlets( String structureINode )
	{
		List<Contentlet> contentlets = null;
	
		try
		{
			// dotCMS API's
			ContentletAPI contentletAPI = APILocator.getContentletAPI();
			UserAPI userAPI = APILocator.getUserAPI();
			User systemUser = userAPI.getSystemUser();

			contentlets = contentletAPI.findByStructure( structureINode, systemUser, true, 0, 0 );
		}
		catch ( Exception exception )
		{
			Logger.error( clazz, "Unable to get all contentlets", exception );
		}

		return contentlets;
	}

    public static List<Contentlet> filterPublished(List<Contentlet> contentlets) {
        List<Contentlet> filteredList = new ArrayList<>();
        for (Contentlet contentlet : contentlets) {
            try {
                if (contentlet.isLive()) {
                    filteredList.add(contentlet);
                }
            } catch ( Exception exception ) {
                Logger.error( clazz, "Unable to filter for live contentlets", exception );
            }
        }
        return filteredList;
    }

}
