<h1>Communication and Writing at Taylor</h1>

<p>Regardless of your career direction, you will be challenged to engage your faith with everything you say, read, write, and create. By including communication and writing classes in the liberal arts curriculum, Taylor calls you to approach conversations and written communication thoughtfully, speak your opinions and ideas, share stories and art, and think critically about the world around you.</p>

<p>Students interested in communication and/or writing careers will find unique homes in our various communication and writing programs. Our Communication, English, and Modern Languages departments are led by academically challenging and nurturing faculty who invest in every student as professionals and fellow followers of Christ.</p>

<p>Students spend four years honing their expertise and exploring their calling by covering a broad range of topics. During your Taylor experience, you will be stretched out of your comfort zone with different writing styles, new ways of thinking, and even other languages.</p>

<div class="is-h3-like">Writing Around the World</div>

<p>Taylor offers both long- and short-term overseas experiences to expand students&rsquo; global awareness of the world of language and teach how to use academic skills to share God&rsquo;s love. Read more about international opportunities related to communication and writing majors.</p>

<div class="is-h5-like">Semester Programs</div>

<ul>
	<li><em>Costa Rica:</em> The Latin American Studies Program offers courses that emphasize the literature of Costa Rican culture and history.</li>
	 <li><em>Lithuania:</em> Immerse yourself in Eastern European culture as you explore this Baltic country and live with international roommates at LCC International University. Though courses can vary by semester, their English and Communication departments have offered introductory literature, advanced writing and literature courses, Teaching English to Speakers of Other Languages, and The Novel.</li>
	 <li><em>United Kingdom, Oxford:</em> This academically robust program is fit for students seeking high level English and Literature studies with a British focus at the University of Oxford.</li>
	 <li><em>United Kingdom, York:</em> Discover literature and creative writing from a new cultural perspective at York St. John University, just outside of London.</li>
	 <li><em>Australia:</em> The Australia Studies Centre (ASC) integrates firsthand observation and study of Australian culture, history, religion, politics, and indigenous cultures together with experiential service learning. Take core major courses in their English department.</li>
	 <li><em>Hong Kong:</em> Spend a semester in one of the top higher educational institutions in Asia and take core communication, journalism, or English-Chinese translation courses.</li>
	 <li><em>New York City:</em> Take your journalism education to the next level with an internship in the Big Apple. Take core journalism courses at King&rsquo;s College and learn what it takes to be a journalist in the big-city news industry.</li>
	 <li><em>Spain:</em> The Semester in Spain program combines academic study with practical opportunities for students to practice what they learn. Modern Languages students are challenged to study in a bilingual environment and build intercultural relationships with fellow students, homestay families, and locals.</li>
</ul>

<div class="is-h5-like">J-Term Programs</div>

<ul>
	<li><em>Literary London:</em> This trip fulfills a literature foundational core requirement. All majors may attend, but those who are studying literature would especially benefit from this trip that allows you to travel to York, Edinburg, Stratford-upon-Avon, and many other locations to study the work of authors like Shakespeare, Jane Austin, T.S. Eliot, C.S. Lewis and others.</li>
	<li><em>Lake Wells, Florida:</em> Join the HEART Institute to learn cross-cultural communication and community development in third-world countries at a simulated village in Florida.</li>
</ul>

<div class="is-h5-like">Spring Break Trips</div>

<ul>
	<li><em>Dominican Republic:</em> Students in media can serve across the Dominican Republic at Students International ministry sites.</li>
</ul>

<div class="is-h3-like">Campus Communication</div>

<p>During your time at Taylor, you will have the ability to gain hands-on experience in your field. You can attend and present papers at conferences, work on the staff of the annual literary magazine, and tutor in the Writing Center.</p>

<ul>
	<li><strong>Parnassus:</strong> Parnassus is an annual literary journal where students can submit their creative work for publication. Every decision&mdash;from selecting and editing works to design and layout&mdash;is made by a student editorial staff.</li>
	<li><strong>Relief Journal:</strong> Taylor&rsquo;s national literary journal, Relief, allows English &mdash; Creative Writing majors to gain literary publishing experience in a yearlong class. Students have the opportunity to work closely with editors and contributors, editing the works of major poets and storytellers, managing social media, and working on the publication side of creative writing.</li>
	<li><strong>Making Literature Conference:</strong> At Taylor&rsquo;s biennial literary conference, well-known authors and scholars are invited to speak to students, faculty, and staff across campus. The conference also features book fairs, discussion panels, and workshops to showcase student work and enhance student writing practices. You can present creative and scholarly papers, lead panels, interact with nationally-known scholars and writers, or volunteer your time to organize and stage the conference.</li>
	<li><strong>Writing Center:</strong> Through employment with the Writing Center, students with distinct strengths in writing can aid their classmates in research paper development, outlining, and revision.</li>
	<li><strong>C.S. Lewis & Friends Colloquium:</strong> Scholars and creative writers from around the country gather every other year to celebrate and commemorate the works of C.S. Lewis, J.R.R. Tolkien, G.K. Chesterton, and others. Students are invited to participate in creative writing sessions, readings, and academic lectures.</li>
	<li><strong>Literary London:</strong> Students from any major who are interested in literature are welcomed to tour York, Edinburg, Stratford-upon-Avon, and many other locations to study the work of authors like Shakespeare, Jane Austin, T.S. Eliot, C.S. Lewis, and others.</li>
	<li><strong>The Flannery O'Connor Society:</strong> Develop research, interview, and writing skills by contributing to The Flannery O'Connor Society's newsletter and blog, an outlet that encourages and supports the literary study of Flannery O'Connor and her creative work.</li>
	<li><strong>Writing Camp:</strong> Gain experience teaching Grant county children and teens (grades 4-12) during summer writing camps. Local students can feel affirmed about their love for reading and writing and meet others who share that passion.</li>
	<li><strong>The Echo:</strong> Participate in the voice-giving mission of Taylor&rsquo;s weekly student-run newspaper&mdash;creating an outlet for campus, local, national, and global news and discussions. Journalistically-inclined writers and editors will gain professional experience by contributing articles, working as a section editor, or managing web content.</li>
	<li><strong>The Ilium:</strong> Taylor University&rsquo;s yearbook is a student-run publication where a team collaborates to produce a publication to preserve Taylor&rsquo;s history and present memories for students to cherish after graduation. Write feature content, capture event photos, and layout pages to tell campus stories.</li>
</ul>

<div class="is-h3-like">Finding the &ldquo;Write&rdquo; Major</div>

<p>Identifying your passions is core to finding the perfect area of study for you. Though many skills can overlap across the department, their practice and application to various communication careers can look very different.</p>

<div><strong>Click on the name of any of the majors below to learn more.</strong></div>

<ul>
	<li><strong>English</strong><br>
	Make reading, writing, and analyzing literary works your career with an English major. Explore unique topics like literary journals, scripts and drama, literary criticism, and the craft and conventions of both academic and imaginative writing.</li>
	<li><strong>English Education</strong><br>
	If you love to share literature and writing with others, you can put your passion for English toward an education field. You will be trained in language arts, lesson planning, and classroom management for the middle school and high school levels.</li>
	<li><strong>Spanish</strong><br>
	Use your foreign language skills to become a proficient communicator and deepen your social and cultural awareness. Study Spanish in-depth to develop fluency, and travel to a Spanish-speaking country to immerse yourself in a cross-cultural experience.</li>
	<li><strong>Spanish Education</strong><br>
	If you have a knack for learning and teaching the Spanish language, you should consider the Spanish Education program. Learn to break down linguistics and explain techniques that will help students understand and love Spanish as much as you do.</li>
	<li><strong>Professional Writing</strong><br>
	Students excited about becoming published writers should consider a Professional Writing degree. Professional Writing majors learn to navigate the world of publishing with many forms of writing&mdash;from fiction novels to magazine articles&mdash;and become published freshman year.</li>
	<li><strong>Multimedia Journalism</strong><br>
	Calling all strategic storytellers&mdash;this major focuses on delivering news and developing creative skills. Prepare to tell real stories not just through writing, but also through design, photography, audio, and video.</li>
	<li><strong>Strategic Communication</strong><br>
	Students interested in the principles that guide effective communication will thrive with a Strategic Communication major. Sharpen your spoken, written, and nonverbal communication skills by studying and applying communication theories.</li>
	<li><strong>Public Relations</strong><br>
	Students interested in managing corporate relationships and handling media communication can develop written and spoken communication skills in our Public Relations major. Study audiences, and message strategies, and gain an understanding of how media ethics hold PR professionals to a high standard.</li>
	<li><strong>Creative Writing*</strong><br>
	Students interested in pursuing writing alongside their declared major can use a Creative Writing minor to bring a novel to life or better communicate ideas. Focus on refining your style and broadening your writing ability across multiple literary styles.</li>
	<li><strong>Literature*</strong><br>
	If you&rsquo;re passionate about reading and critically analyzing literary works, then consider adding a Literature minor to your degree. Discover how literary works were both expressions and influencers of culture and society.</li>
</ul>

<div><em>*Indicates that the program is only a minor.</em></div>

<p><a href="/majors">Explore a full list of majors.</a></p>