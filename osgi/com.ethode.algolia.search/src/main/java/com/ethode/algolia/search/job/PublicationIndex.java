package com.ethode.algolia.search.job;

import com.algolia.search.*;
import com.algolia.search.objects.tasks.sync.*;

import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Relationship;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.Logger;

import com.ethode.algolia.search.classes.Publication;
import com.ethode.algolia.search.classes.Page;
import com.ethode.algolia.search.key.AlgoliaImporterKey;
import com.ethode.algolia.search.utilities.ContentletUtility;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Justin Kubicek on 4/14/2018.
 */
public class PublicationIndex
{
	public static Class<PublicationIndex> clazz = PublicationIndex.class;

	private static final String APPLICATIONID = AlgoliaImporterKey.APPLICATIONID;
	private static final String SEARCHONLYAPIKEY = AlgoliaImporterKey.SEARCHONLYAPIKEY;
	private static final String ADMINAPIKEY = AlgoliaImporterKey.ADMINAPIKEY;
	private static final String PUBLICATIONCONTENTTYPEIDENTIFIER = AlgoliaImporterKey.PUBLICATIONCONTENTTYPEIDENTIFIER;

	public static void create()
	{
		Logger.info(clazz, "Starting create indexes.");

		//https://www.algolia.com/doc/api-client/java/getting-started/
		Logger.info(clazz, "Setting up networking...");
		java.security.Security.setProperty("networkaddress.cache.ttl", "60");
		// Connect to Algolia
		Logger.info(clazz, "Setting up Algolia client...");
		APIClient algoliaclient = new ApacheAPIClientBuilder(APPLICATIONID, ADMINAPIKEY).build();
		Logger.info(clazz, "Create a temporary index...");
		Index<Publication> algoliaindex = algoliaclient.initIndex( "tmp_publication_index", Publication.class );
		Logger.info(clazz, "Index Ready.");

		Logger.info(clazz, "Get all dotCMS Publications");
		List<Contentlet> dotCMSPublications = ContentletUtility.filterPublished( ContentletUtility.listContentlets( PUBLICATIONCONTENTTYPEIDENTIFIER ) );
		Logger.info(clazz, "Build a list for Algolia");
		List<Publication> publications = createAnAlgoliaPublicationList( dotCMSPublications );
		Logger.info(clazz, "Try adding these to the index");

		try{
			TaskSingleIndex task = algoliaindex.addObjects(publications);
			// task.waitForCompletion();
			Logger.info(clazz, "Publications were added.");
		} catch( Exception e ){
			Logger.info(clazz, "AlgoliaException: " + e);
		}

		// Then move the new index into place
		// Rename tmp_publication_index to MyIndex (and overwrite it)
		try{
			algoliaindex.moveTo("publication_pages");
			// task.waitForCompletion();
			Logger.info(clazz, "Moved the new index to publication_pages");
		} catch( Exception e ){
			Logger.info(clazz, "Error moving the new index in place: " + e);
		}
		

	}

	public static List<Publication> createAnAlgoliaPublicationList ( List<Contentlet> publications )
	{
		List<Publication> algoliaPublicationList = new ArrayList<Publication>();
		String friendlyname = "";
		String seodescription = "";
		String uri = "";

		for ( Contentlet publication : publications )
		{
			try{
				if( publication.getStringProperty( "friendlyname" ) == null ){
					friendlyname = "";
				}else{
					friendlyname = publication.getStringProperty( "friendlyname" );
				}
			}catch (Exception e ){
				friendlyname = "";
			}
			try{
				if( publication.getStringProperty( "seodescription" ) == null ){
					seodescription = "";
				}else{
					seodescription = publication.getStringProperty( "seodescription" );
				}
			}catch (Exception e ){
				seodescription = "";
			}
			try{
				if( publication.getStringProperty( "urlTitle" ) == null ){
					uri = "";
				}else{
					uri = "/research/" + publication.getStringProperty( "urlTitle" );
				}
			}catch (Exception e ){
				uri = "";
			}
			if ( friendlyname != ""){

				algoliaPublicationList.add( new Publication().setPageTitle( friendlyname )
				.setPage_Meta_Description( seodescription )
				.setCategory( "Publications" )
				.setPage_Uri( uri ) );

			}
			
			
		}

		return algoliaPublicationList;
	}

}