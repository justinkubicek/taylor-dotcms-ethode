package com.ethode.jkubicek.rest.beans;

public enum RESTParameters
{
	AFFINITYGROUPS          ( "affinitygroups" ),;

	public static Class<RESTParameters> clazz = RESTParameters.class;

	private final String value;

	RESTParameters( String value )
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

	@Override
	public String toString()
	{
		return value;
	}
}
