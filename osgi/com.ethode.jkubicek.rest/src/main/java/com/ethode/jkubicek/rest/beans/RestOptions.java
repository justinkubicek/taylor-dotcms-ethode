package com.ethode.jkubicek.rest.beans;

import com.dotcms.rest.InitDataObject;
import com.dotmarketing.business.APILocator;
import com.dotmarketing.portlets.languagesmanager.business.LanguageAPI;
import com.dotmarketing.util.UtilMethods;

import com.liferay.portal.model.User;

import com.ethode.jkubicek.rest.beans.RESTParameters;
import com.ethode.jkubicek.rest.log.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RestOptions
{
	public static Class<RestOptions> clazz = RestOptions.class;

	String affinitygroups = "";

	public RestOptions( InitDataObject initDataObject )
	{
		Logger.info( clazz, "Got to RestOptions( InitDataObject )" );

		Map<String, String> parameterMap = initDataObject.getParamsMap();

		debug( parameterMap );

		this.affinitygroups = parameterMap.get( RESTParameters.AFFINITYGROUPS.getValue() );

	}

	private void debug( Map<String, String> parameterMap )
	{
		Logger.info( this, RESTParameters.AFFINITYGROUPS.getValue() + " = " + parameterMap.get( RESTParameters.AFFINITYGROUPS.getValue() ) );
	}


	public String getAffinitygroups()
	{
		return this.affinitygroups;
	}
}
