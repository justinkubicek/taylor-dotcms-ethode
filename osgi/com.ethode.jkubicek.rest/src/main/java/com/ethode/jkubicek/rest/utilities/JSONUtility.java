package com.ethode.jkubicek.rest.utilities;

//import com.dotcms.contenttype.business.ContentTypeAPI;
//import com.dotcms.contenttype.model.type.ContentType;
//import com.dotmarketing.business.APILocator;
//import com.dotmarketing.business.UserAPI;
import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.UtilMethods;
import com.dotmarketing.util.json.JSONArray;
import com.dotmarketing.util.json.JSONException;
import com.dotmarketing.util.json.JSONObject;

// import com.ethode.jkubicek.rest.beans.FieldFilter;

//import com.liferay.portal.model.User;

import com.ethode.jkubicek.rest.log.Logger;

import java.util.List;

public class JSONUtility
{
	public static Class<JSONUtility> clazz = JSONUtility.class;

	public static JSONObject contentletToJSONObject( Contentlet contentlet ) throws JSONException
	{
		return contentletToJSONObject( contentlet, null );
	}

	public static JSONObject contentletToJSONObject( Contentlet contentlet, String contentTypeVariable ) throws JSONException
	{
		JSONObject jsonObject = new JSONObject();

		try
		{
			if ( !UtilMethods.isSet( contentTypeVariable ) )
			{
				// dotCMS API's
				// 4.x code
				//UserAPI userAPI = APILocator.getUserAPI();
				//User systemUser = userAPI.getSystemUser();
				//ContentTypeAPI contentTypeAPI = APILocator.getContentTypeAPI( systemUser );

				// 4.x code
				//ContentType contentType = contentTypeAPI.find( contentlet.getContentTypeId() );
				//String contentTypeVariable = contentType.variable();

				// 3.x code
				Structure contentType = contentlet.getStructure();

				// 3.x code
				contentTypeVariable = contentType.getVelocityVarName();
			}

            jsonObject.put( "Content Type", contentTypeVariable );
            jsonObject.put( "identifier", contentlet.getMap().get( "identifier" ) );
            jsonObject.put( "title", contentlet.getMap().get( "title" ) );
			jsonObject.put( "uniqueid", contentlet.getMap().get( "uniqueid" ) );
			jsonObject.put( "type", contentlet.getMap().get( "type1" ) );
            
		}
		catch ( Exception exception )
		{
			Logger.error( clazz, "Error converting contentlet to JSONObject.", exception );
		}

		return jsonObject;
	}

	public static JSONArray contentletsToJSONArray( List<Contentlet> contentlets )
	{
		return contentletsToJSONArray( contentlets, null );
	}

	public static JSONArray contentletsToJSONArray( List<Contentlet> contentlets, String contentTypeVariable )
	{
		JSONArray jsonArray = new JSONArray();

		try
		{
			for ( Contentlet contentlet : contentlets )
			{
				JSONObject jsonObject = contentletToJSONObject( contentlet, contentTypeVariable );

				if ( UtilMethods.isSet( jsonObject ) )
				{
					jsonArray.add( jsonObject );
				}
			}
		}
		catch ( Exception exception )
		{
			Logger.error( clazz, "Error converting contentlets to JSONArray.", exception );
		}

		return jsonArray;
	}
}