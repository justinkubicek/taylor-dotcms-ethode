package com.ethode.algolia.search.job;

import com.algolia.search.*;
import com.algolia.search.objects.tasks.sync.*;

//import com.dotmarketing.portlets.contentlet.model.Contentlet;
// import com.dotmarketing.util.Logger;

import com.ethode.algolia.search.classes.Course;
import com.ethode.algolia.search.classes.Page;
import com.ethode.algolia.search.job.CourseIndex;
import com.ethode.algolia.search.job.EventIndex;
import com.ethode.algolia.search.job.NewsIndex;
import com.ethode.algolia.search.key.AlgoliaImporterKey;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Justin Kubicek on 2/18/2018.
 * Adpoted from code written by Nathan Keiter (nkeiter) on 12/13/2017.
 */
public class CreateIndexes
{
	public static Class<CreateIndexes> clazz = CreateIndexes.class;

	private static final String APPLICATIONID = AlgoliaImporterKey.APPLICATIONID;
	private static final String SEARCHONLYAPIKEY = AlgoliaImporterKey.SEARCHONLYAPIKEY;
	private static final String ADMINAPIKEY = AlgoliaImporterKey.ADMINAPIKEY;

	public static void createIndexes()
	{

		CourseIndex.create();
		PagesIndex.create();
		EventIndex.create();
		NewsIndex.create();
		PeopleIndex.create();
		PublicationIndex.create();
		
	}
}
