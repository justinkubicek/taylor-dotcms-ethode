/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//import "babel-polyfill";

window.Vue = require("vue");
//https://momentjs.com/docs/
var moment = require("moment");
//https://www.npmjs.com/package/sort-json-array
var sortJsonArray = require("sort-json-array");

Vue.component(
  "pulse-loader",
  require("../../../node_modules/vue-spinner/src/PulseLoader.vue")
);

//https://www.npmjs.com/package/vue-scrollto
var VueScrollTo = require("vue-scrollto");
Vue.use(VueScrollTo);

VueScrollTo.setDefaults({
  container: "body",
  duration: 500,
  easing: "ease",
  offset: -50,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
});
/* This may not be the best way to do this. Slick was not loading. */
var $ = require("jquery");
window.jQuery = $;
window.$ = $;
/* https://stackoverflow.com/questions/28969861/managing-jquery-plugin-dependency-in-webpack */

var slick = require("slick-carousel");

import InstantSearch from "vue-instantsearch";
//import VueLazyLoad from 'vue-lazyload';
import Slideout from "vue-slideout";

import contactsList from "./components/contactsList";
import SubnavBar from "./components/SubnavBar";
import SlideoutNavSection from "./components/SlideoutNavSection";

// FontAwesome5 Icons (importing each manually to decrease browser download )
// import fontawesome from '@fortawesome/fontawesome'
// import faPhone from '@fortawesome/fontawesome-free-solid/faPhone'
// import faEnvelope from '@fortawesome/fontawesome-free-solid/faEnvelope'
// import faHome from '@fortawesome/fontawesome-free-solid/faHome'
// import faAddressBook from '@fortawesome/fontawesome-free-solid/faAddressBook'
// import faMap from '@fortawesome/fontawesome-free-solid/faMap'
// import faUsers from '@fortawesome/fontawesome-free-solid/faUsers'
// import faFlag from '@fortawesome/fontawesome-free-solid/faFlag'
// import faStar from '@fortawesome/fontawesome-free-solid/faStar'
// import faInfoCircle from '@fortawesome/fontawesome-free-solid/faInfoCircle'
// import faCompass from '@fortawesome/fontawesome-free-solid/faCompass'
// import faCalendar from '@fortawesome/fontawesome-free-solid/faCalendar'
// import faHeadphones from '@fortawesome/fontawesome-free-solid/faHeadphones'
// import faPlus from '@fortawesome/fontawesome-free-solid/faPlus'
// import faSearch from '@fortawesome/fontawesome-free-solid/faSearch'
// fontawesome.library.add(faPhone);
// fontawesome.library.add(faEnvelope);
// fontawesome.library.add(faHome);
// fontawesome.library.add(faAddressBook);
// fontawesome.library.add(faMap);
// fontawesome.library.add(faUsers);
// fontawesome.library.add(faFlag);
// fontawesome.library.add(faStar);
// fontawesome.library.add(faCalendar);
// fontawesome.library.add(faHeadphones);
// fontawesome.library.add(faPlus);
// fontawesome.library.add(faSearch);
// fontawesome.library.add(faInfoCircle);
// fontawesome.library.add(faCompass);

Vue.use(InstantSearch);
//Vue.use(VueLazyLoad);
Vue.component("contacts-list", contactsList);
Vue.component("subnav-bar", SubnavBar);
Vue.component("slideout-nav-section", SlideoutNavSection);
Vue.filter("capitalize", function(value) {
  return value.toUpperCase();
});

//Manually show that the browser is outdated
var isoutdated = document.getElementById("is-outdated");
var outdated = document.getElementById("outdated");

if( outdated != null && isoutdated != null && outdated.style.opacity != 100){
outdated.style.opacity = 100;
outdated.style.filter = "alpha(opacity=100)";
outdated.style.display = 'block';
var btnClose = document.getElementById("btnCloseUpdateBrowser");
btnClose.onmousedown = function() {
  outdated.style.display = 'none';
  return false;
};
}

/**
 * We're going to add the code for all of the sliders here
 */
var personalImages = document.getElementById("personal-images");
if (personalImages != null) {
$('#personal-images').slick({
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 3000,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true
});
}


var singleItem = document.getElementById("single-item");
if (singleItem != null) {
$('#single-item').slick({
  dots: false,
  infinite: true,
  autoplay: false,
  slidesToShow: 1,
  arrows: true,
  nextArrow: '<button type="button" class="taylor-next" aria-label="Next">Next</button>',
  prevArrow: '<button type="button" class="taylor-prev" aria-label="Previous">Previous</button>',
});
}


var homeNewsCarouselItems = document.getElementById("home-news-carousel-items");
if (homeNewsCarouselItems != null) {
$('#home-news-carousel-items').slick({
  infinite: true,
  autoplay: false,
  slidesToShow: 1,
  dots: true,
  adaptiveHeight: true,
  appendDots: $('.carousel-index'),
  nextArrow: $('.next-button'),
  prevArrow: $('.prev-button'),
/////
dotsClass: 'taylor-dots', //slick generates this <ul.custom-dots> within the appendDots container
    customPaging: function (slider, i) {
        //FYI just have a look at the object to find aviable information
        //press f12 to access the console
        //you could also debug or look in the source
        console.log(slider);
        var slideNumber = (i + 1),
            totalSlides = slider.slideCount;
            return '<button type="button" class="dot" aria-label="' + slideNumber + ' of ' + totalSlides + '" title="' + slideNumber + ' of ' + totalSlides + '">' + slideNumber + '</button>';
        }
/////
});
}


//Done with Sliders

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

var app = new Vue({
  el: "#app",
  components: {
    contactsList,
    SubnavBar,
    Slideout,
    SlideoutNavSection
  },
  data: function() {
    return {
      // breakpoints
      mobileBreakpoint: 768,
      tabletBreakpoint: 1023,
      desktopBreakpoint: 1215,

      // Instantsearch Variables
      query: "", // Algolia Instantsearch Query
      showModal: false,

      // Mobile Search Variables
      showSmallSearchBar: false,

      // Auto-hide Nav Bar Variables
      lastScrollTop: 0,
      delta: 250,
	    fullNavShown: true,

      fixedHeader: document.getElementById("full-nav"),

    };
  },
  computed: {
    navBurgerClassObject: function() {
      return {
        //'is-active': app.Slideout.data._opened
      };
    }
  },
  created: function(){
	  window.addEventListener("resize", function() {
		  app.adjustPageTopPadding();
		  app.interchangeImage();
		  // app.setSearchResultsPosition();
		  app.unsetMobileSearchOnDesktop(); // Turn off Mobile Search feature when resizing from mobile to tablet/desktop
		  app.checkAndSetMobileSearch(); // Turn on Mobile Search feature when resizing from tablet/desktop to mobile and query is set
      app.showFullNav();
      app.showHideSlideOut();
	  });
	  window.addEventListener("scroll", function() {
		  //console.log(slideout.data._opened)
      app.setSearchResultsPosition();
	  });
  },
  mounted: function() {
    this.liveChat();
    this.lazyLoadYoutubeVideos();
	  this.adjustPageTopPadding();
	  this.interchangeImage();
	  this.showFullNav();
	  this.checkDidScroll(250); // runs every xxx miliseconds'
	  this.ifNavBarExistsRemoveHeaderShadow();
	  this.setSearchResultsPosition();
  },
  watch: {
    showModal: function(newVal) {
      // add Bulma's is-clipped' class to html tag when search modal is open
      var className = "is-clipped";
      if (newVal) {
        document.documentElement.classList.add(className);
        Vue.nextTick(function() {
          document.getElementById("modal-search-input").focus();
        });
      } else {
        document.documentElement.classList.remove(className);
      }
    },
    showSmallSearchBar: function(newVal) {
      if (newVal) {
        Vue.nextTick(function() {
          document.getElementById("search-input-small").focus();
        });
      }
    }
  },
  methods: {
    // LiveChat (www.livechatinc.com) code
    liveChat: function() {
        window.__lc = window.__lc || {};
        window.__lc.license = 8894864;
        (function() {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();
    },
    isMobile: function() {
      return document.documentElement.clientWidth <= this.mobileBreakpoint;
    },
    isTablet: function() {
      return (
        document.documentElement.clientWidth <= this.tabletBreakpoint &&
        document.documentElement.clientWidth > this.mobileBreakpoint
      );
    },
    isDesktop: function() {
      return (
        document.documentElement.clientWidth <= this.desktopBreakpoint &&
        document.documentElement.clientWidth > this.tabletBreakpoint
      );
    },
    adjustPageTopPadding: function() {
      var pageContent = document.getElementById("page-content");
      var mobileTagline = document.getElementById("tagline-mobile");

      if (this.isMobile()) {
        var navBar;
        if (!this.showSmallSearchBar) {
          // If we're not showing the mobile search bar, use the bottom
          // of the nav-inital div to set the margin-top of the page-content div
          var navBar = document.getElementById("nav-initial");
          var navBarBottomPosition = navBar.getBoundingClientRect().bottom;
          pageContent.style.marginTop = navBarBottomPosition + "px";
        } else {
          // If we are showing the mobile search bar, simply set the margin-top
          // of the page-content div to 44px
          pageContent.style.marginTop = "44px";
        }
        if (pageContent && navBar) {
          //var navBarBottomPosition = navBar.offsetHeight;
          var navBarBottomPosition = navBar.getBoundingClientRect().bottom;
          pageContent.style.marginTop = navBarBottomPosition + "px";
        }
      } else {
        pageContent.removeAttribute("style");
      }
    },
    setMobileSearchSize() {
      this.showSmallSearchBar = true;
      var navDuringSearch = document.getElementById("nav-during-search");
      var navInitial = document.getElementById("nav-initial");
      var pageContent = document.getElementById("page-content");
      if (pageContent && navDuringSearch) {
        var pageContentTopPosition = pageContent.getBoundingClientRect().top;
        //console.log(pageContentTopPosition);
        navInitial.style.display = "none";
        navDuringSearch.style.display = "block";
        //navDuringSearch.style.height = pageContentTopPosition + "px";
      }
      this.adjustPageTopPadding();
    },
    unsetMobileSearchSize() {
      this.showSmallSearchBar = false;
      var navDuringSearch = document.getElementById("nav-during-search");
      var navInitial = document.getElementById("nav-initial");
      if (navDuringSearch && navInitial) {
        navDuringSearch.style.display = "none";
        //navDuringSearch.removeAttribute('style');
        navInitial.style.display = "block";
      }
      this.adjustPageTopPadding();
    },
    unsetMobileSearchOnDesktop() {
      if (app.isTablet()) {
        this.unsetMobileSearchSize();
      }
    },
    checkAndSetMobileSearch() {
      if (this.isMobile() && this.query != "") {
        this.setMobileSearchSize();
      }
    },
    // BEGIN IMAGE INTERCHANGE METHODS
    // (designed to work with Foundation data-interchange html syntax)
    interchangeImage: function() {
      // Note: these breakpoints are currently based on Foundation's Breakpoints, not on Bulma's
      var clientWidth = document.documentElement.clientWidth;
      if (clientWidth < 640) {
        // small
        this.placeImage("small");
      } else if (clientWidth < 1024) {
        // medium
        this.placeImage("medium");
      } else if (clientWidth < 1200) {
        // large
        this.placeImage("large");
      } else if (clientWidth < 1440) {
        // xlarge
        this.placeImage("xlarge");
      } else {
        // xxlarge
        this.placeImage("xxlarge");
      }
    },
    placeImage(screenWidth) {
      // Change Background Image based on Viewport Width
      document.querySelectorAll("[data-interchange]").forEach(function(el) {
        // Loop through each Data-Interchange element
        var imageArrayString = el.dataset.interchange; // Get data stored in data-interchange
        var imageArray = imageArrayString.split("]"); // Turn data into an array
        for (var i = 0, len = imageArray.length; i < len; i++) {
          // Loop through each item in the new array
          var indexOfComma = imageArray[i].indexOf(",", 5); // Find the index of the comma
          if (imageArray[i].search(screenWidth) != -1) {
            // Determine whether or not the current item is for the intended screen width
            var imageRoute = imageArray[i].substring(0, indexOfComma); // Grab the route section of the current item
            imageRoute = imageRoute.replace("[", ""); // Remove Opening Brackets from route
            imageRoute = imageRoute.replace(/\s+/g, ""); // Remove Spaces from route
            imageRoute = imageRoute.replace(",", ""); // Remove Commas from route
            if (el.tagName == "DIV") {
              // Determine whether or not the element with the data-interchange attribute is a div
              var imageStyles = 'url("' + imageRoute + '")'; // Set up style for background image
              el.style.backgroundImage = imageStyles; // Add style to the html element
            } else if (el.tagName == "IMG") {
              // Determine whether or not the element with the data-interchange attribute is an img
              el.src = imageRoute; // Add src to html element
            }
            return;
          }
        }
      });
    },
    lazyLoadYoutubeVideos(){
      var youtube = document.querySelectorAll( ".youtube" );
  
      for (var i = 0; i < youtube.length; i++) {
        
        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
        
        var image = new Image();
        image.src = source;
        image.addEventListener( "load", function() {
          youtube[ i ].appendChild( image );
        }( i ) );
    
        youtube[i].addEventListener( "click", function() {

          var iframe = document.createElement( "iframe" );

              iframe.setAttribute( "frameborder", "0" );
              iframe.setAttribute( "allowfullscreen", "" );
              iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1");

              this.innerHTML = "";
              this.appendChild( iframe );
        } );  
      };
    },
    // END IMAGE INTERCHANGE METHODS

    // BEGIN SEARCH RESULTS POSITION METHODS
    setSearchResultsPosition: function() {
      var searchBar = document.getElementById("nav-large-search-container");
      var searchResults = document.getElementById("search-dropdown-container");
      if (searchBar && searchResults) {
        var searchBarPosition = searchBar.getBoundingClientRect();
        searchResults.style.left = searchBarPosition.left + "px";
        if (this.isMobile()) {
          searchResults.style.removeProperty('top');
        }
        else {
          searchResults.style.top = searchBarPosition.bottom + "px";
        }
        searchResults.style.height =
          window.innerHeight - searchBarPosition.bottom + "px";
      }
    },
    // END SEARCH RESULTS POSITION METHODS

    ifNavBarExistsRemoveHeaderShadow: function() {
      if (document.getElementById("subnav-bar")) {
        document.getElementById("full-nav").style.boxShadow = "none";
      }
    },

	// BEGIN HIDE HEADER ON SCROLL METHODS
	// https://medium.com/@mariusc23/hide-header-on-scroll-down-show-on-scroll-up-67bbaae9a78c (modified)
	  checkDidScroll: function(miliseconds) {
		  this.setFullNav();
		  setTimeout(this.checkDidScroll, miliseconds);
	  },
	  showFullNav: function() {
		  var fullNav = document.getElementById("full-nav");
		  var subNav = document.getElementsByClassName("navbar")[0];
      // var fullNavHeight =  document.getElementById("top-nav-full").offsetHeight + document.getElementById("nav-initial").offsetHeight;
      var fullNavHeight =  document.getElementById("top-nav-full") + document.getElementById("nav-initial");
		  fullNav.style.top = "0px";
		  if (subNav) {
			  subNav.style.top = fullNavHeight + "px";
		  }
		  this.fullNavShown = true;
		  console.log("showFullNav called");
	  },
	  hideFullNav: function() {
		  var fullNav = document.getElementById("full-nav");
		  var subNav = document.getElementsByClassName("navbar")[0];
      // var fullNavHeight = document.getElementById("top-nav-full").offsetHeight + document.getElementById("nav-initial").offsetHeight;
      var fullNavHeight = document.getElementById("top-nav-full") + document.getElementById("nav-initial");
		  fullNav.style.top = "-" + fullNavHeight + "px";
          if (subNav) {
		      subNav.style.top = "0px";
          }
		  this.query = "";
		  this.fullNavShown = false;
		  console.log("hideFullNav called")
	  },
	  setFullNav: function() {
		  // this function checks the scroll situation and hide/show nav bar accordingly
		  // should be called regularly

		  // make sure user is not on mobile device
		  if (document.documentElement.clientWidth <= this.mobileBreakpoint) return;

		  var st = document.documentElement.scrollTop;
		  // Make sure they scroll more than delta
		  if (Math.abs(this.lastScrollTop - st) <= this.delta) return;

		  var fullNav = document.getElementById("full-nav");
      // var fullNavHeight = document.getElementById("top-nav-full").offsetHeight + document.getElementById("nav-initial").offsetHeight;
      var fullNavHeight = document.getElementById("top-nav-full") + document.getElementById("nav-initial");
		  if (st > this.lastScrollTop) 			// if scrolling down
		  {
			  if (st > fullNavHeight &&				// if scrolling more than the full nav height
					  this.fullNavShown) 				// if fullnav is shown
			  {
				  this.hideFullNav();			// Hide full-nav
			  }
		  } 
		  else									// if scrolling up
		  {
			  if (!this.fullNavShown) // if full nav is hidden
			  {
				  this.showFullNav(); // show full-nav
			  }
		  }
		  this.lastScrollTop = st;
    },
    // END HIDE HEADER ON SCROLL METHODS

    // START SLIDEOUT.JS METHODS
    slideoutOnTranslate: function(translated) {
      var newFixedHeader = document.getElementById("full-nav");
      newFixedHeader.style.transform = "translateX(" + translated + "px)";
    },
    slideoutOnBeforeOpen: function() {
      var newFixedHeader = document.getElementById("full-nav");
      newFixedHeader.style.transition = "transform 300ms ease";
      newFixedHeader.style.transform = "translateX(-256px)";

      var slideoutToggleOne = document.getElementsByClassName("navbar-burger")[0];
      slideoutToggleOne.classList.add("is-active");
    },
    slideoutOnBeforeClose: function() {
      var newFixedHeader = document.getElementById("full-nav");
      newFixedHeader.style.transition = "transform 300ms ease";
      newFixedHeader.style.transform = "translateX(0px)";

      var slideoutToggleOne = document.getElementsByClassName("navbar-burger")[0];
      slideoutToggleOne.classList.remove("is-active");
    },
    slideoutOnOpen: function() {
      var newFixedHeader = document.getElementById("full-nav");
      newFixedHeader.style.transition = "";
    },
    slideoutOnClose: function() {
      var newFixedHeader = document.getElementById("full-nav");
      newFixedHeader.style.transition = "";
    },
    showHideSlideOut: function() {
      var newFixedHeader = document.getElementById("full-nav");
      var slideoutToggleOne = document.getElementsByClassName("navbar-burger")[0];
      var panel = document.getElementById("panel");
      if (!this.isMobile()) {
        var newFixedHeader = document.getElementById("full-nav");
        newFixedHeader.style.transition = "transform 300ms ease";
        newFixedHeader.style.transform = "translateX(0px)";
        slideoutToggleOne.classList.remove("is-active");
        newFixedHeader.style.transition = "";
        panel.style.transform = "translateX(0px)";
      } else if (slideoutToggleOne.classList.contains("is-active")) {
        newFixedHeader.style.transition = "";
        panel.style.transition = "";
        newFixedHeader.style.transform = "translateX(-256px)";
        panel.style.transform = "translateX(-256px)";
        //slideoutToggleOne.classList.add('is-active');
      }
    }
  }
});

//Check to see if the news app is there
var newsPageWidget = document.getElementById("news-page-widget");
if (newsPageWidget != null) {
  var newsApiURL =
    "/api/content/render/false/type/json/query/+contentType:NewsItem +languageId:1 +deleted:false +live:true +working:true/orderby/NewsItem.publishDate desc/limit/2000";
  var newsPageWidget = new Vue({
    el: "#news-page-widget",
    data: function() {
      return {
        newsitems: null,
        sorttopic: null,
        limitTopics: 7,
        totalTopicCount: 0,
        showMore: false,
        initialQtyOfNewsItemsToShow: 5,
        qtyOfNewsItemsToShow: 5,
        showMoreIncrement: 5,
        searchquery: "",
        browsebytopic: false,
        browsebytime: false,
        filtermonth: null,
        howold: 0,
        isFiltered: false
      };
    },
    computed: {
      topics: function() {
        //   var arrayOfTopics = [
        //     { name: "Commencement", count: 24, href: "/" },
        //     { name: "Cybersecurity", count: 21, href: "/" },
        //     { name: "Entrepreneurship", count: 32, href: "/" },
        //     { name: "Enviroment", count: 130, href: "/" },
        //     { name: "Health", count: 186, href: "/" },
        //     { name: "Interdisciplinary", count: 83, href: "/" },
        //     { name: "International", count: 223, href: "/" },
        //     { name: "K-12 Education", count: 43, href: "/" }
        //   ];
        //Allow the user to filter by newsType
        var listTopics = this.listTopics();
        var arrayOfTopics = this.arrayOfTopics(listTopics);
        if (arrayOfTopics != null) {
          this.totalTopicCount = arrayOfTopics.length;
        }
        //console.log(arrayOfTopics);
        return sortJsonArray(arrayOfTopics, "name", "asc").splice(
          0,
          this.limitTopics
        );
      },
      filterMonths: function() {
        var months = [];
        var i = 0;
        var shiftDates = this.howold * 12;
        while (i < 12) {
          months.push(
            moment()
              .subtract(i + shiftDates, "months")
              .format("MMM YYYY")
          );
          i++;
        }
        return months;
      },
      filterednewsitems: function() {
        var filterednewsitems = [];
        var self = this;
        if (this.searchquery) {
          //console.log( this.searchquery );
          filterednewsitems = self.newsitems.filter(newsitem =>
            this.applyquery(newsitem)
          );
        } else if (self.sorttopic != null && self.newsitems != null) {
          self.newsitems.forEach(function(newsitem) {
            var newsTypes = newsitem.newsType.split(", ");
            if (newsTypes != null) {
              newsTypes.forEach(function(topic) {
                if (topic === self.sorttopic) {
                  filterednewsitems.push(newsitem);
                }
              });
            }
          });
        } else if (self.filtermonth != null && self.newsitems != null) {
          var filtermonthMonth = moment(self.filtermonth, "MMM YYYY").format(
            "MMM"
          );
          var filtermonthYear = moment(self.filtermonth, "MMM YYYY").format(
            "YYYY"
          );
          filterednewsitems = self.newsitems.filter(newsitem =>
            this.filterNewsItemsByMonth(
              newsitem,
              filtermonthMonth + filtermonthYear
            )
          );
        } else {
          filterednewsitems = self.newsitems;
        }
        return sortJsonArray(filterednewsitems, "publishDate", "des");
      },
      displayednewsitems: function() {
        var displayednewsitems = [];
        var self = this;
        if (self.filterednewsitems.length > self.qtyOfNewsItemsToShow) {
          for (var item, i = 0; (item = self.filterednewsitems[i++]); ) {
            if (i <= self.qtyOfNewsItemsToShow) {
              displayednewsitems.push(self.filterednewsitems[i]);
            } else {
              break;
            }
          }
        } else {
          displayednewsitems = self.filterednewsitems;
        }
        return displayednewsitems;
      },
      newsLoading: function() {
        //This line may be able to be reduced to the following
        //return (this.displayednewsitems != null && this.displayednewsitems.length > 0);
        if (
          this.newsitems != null &&
          this.newsitems.length > 0
        ) {
          return false;
        } else {
          return true;
        }
      }
    },
    created: function() {
      this.fetchData();
    },
    // watch: {
    //   //
    // },
    filters: {
      formatDate: function(v) {
        //return v.replace(/T|Z/g, ' ')
        return moment(v).format("MMMM Do, YYYY");
      },
      truncateAbstract: function(str) {
        var length = 170;
        var ending = "...";
        if (str.length > length) {
          return str.substring(0, length - ending.length) + ending;
        } else {
          return str;
        }
      },
      truncateHeadline: function(str) {
        var length = 40;
        var ending = "...";
        if (str.length > length) {
          return str.substring(0, length - ending.length) + ending;
        } else {
          return str;
        }
      }
    },
    methods: {
      fetchData: function() {
        var xhr = new XMLHttpRequest();
        var self = this;
        xhr.open("GET", newsApiURL);
        xhr.onload = function() {
          self.newsitems = JSON.parse(xhr.responseText).contentlets;
          //app.newsitems = JSON.parse(xhr.responseText).contentlets
          //console.log(self.newsitems.length)
        };
        xhr.send();
      },
      listTopics: function() {
        var allNewsTypes = "";
        if (this.newsitems != null) {
          this.newsitems.forEach(function(newsitem) {
            if (allNewsTypes.length > 0 && allNewsTypes != null) {
              allNewsTypes = allNewsTypes + ", " + newsitem.newsType;
            } else {
              allNewsTypes = newsitem.newsType;
            }
            //console.log( newsitem.newsType );
          });
          //console.log(allNewsTypes);
          return allNewsTypes;
        }
      },
      arrayOfTopics: function(listTopics) {
        if (listTopics != null) {
          var arrayOfTopics = [];
          var topicsStored = [];
          var arrayOfTopicsFromString = listTopics.split(", ");

          for (var topic, i = 0; (topic = arrayOfTopicsFromString[i++]); ) {
            if (topicsStored.indexOf(topic) <= -1) {
              var arrayTopic = [];
              arrayTopic["name"] = topic;
              arrayTopic["count"] = this.countTopic(
                topic,
                arrayOfTopicsFromString
              );
              topicsStored.push(topic);
              arrayOfTopics.push(arrayTopic);
            }
          }
          // console.log(arrayOfTopics);
          return arrayOfTopics;
        } else {
          return null;
        }
      },
      countTopic: function(topic, listTopics) {
        if (listTopics.length > 0 && listTopics != null) {
          var count = 0;
          listTopics.forEach(function(listedTopic) {
            if (listedTopic === topic) {
              count++;
            }
          });
          return count;
        } else {
          console.log("error");
          return 0;
        }
      },
      sortByTopic: function(topic) {
        this.removeFilters();
        this.isFiltered = true;
        this.searchquery = "";
        this.sorttopic = topic;
      },
      removeFilters: function() {
        this.browsebytopic = false;
        this.browsebytime = false;
        this.isFiltered = false;
        this.sorttopic = null;
        this.limitTopics = 7;
        this.showMore = false;
        this.filtermonth = null;
        this.howold = 0;
        this.qtyOfNewsItemsToShow = this.initialQtyOfNewsItemsToShow;
      },
      removeFiltersOnClick: function() {
        this.removeFilters();
        this.searchquery = "";
      },
      showHideTopics: function() {
        if (this.limitTopics === this.totalTopicCount) {
          this.limitTopics = 7;
          this.showMore = false;
        } else {
          this.limitTopics = this.totalTopicCount;
          this.showMore = true;
        }
      },
      showMoreNews: function() {
        this.qtyOfNewsItemsToShow =
          this.qtyOfNewsItemsToShow + this.showMoreIncrement;
      },
      applyquery: function(newsitem) {
        this.removeFilters();
        this.isFiltered = true;
        if (
          newsitem.headline
            .toLowerCase()
            .indexOf(this.searchquery.toLowerCase()) > -1 ||
          newsitem.shortSummary
            .toLowerCase()
            .indexOf(this.searchquery.toLowerCase()) > -1 ||
          newsitem.body.toLowerCase().indexOf(this.searchquery.toLowerCase()) >
            -1
        ) {
          return true;
        } else {
          return false;
        }
      },
      showHidebrowsebytopic: function() {
        if (this.browsebytopic) {
          this.browsebytopic = false;
        } else {
          this.browsebytopic = true;
        }
      },
      showHidebrowsebytime: function() {
        if (this.browsebytime) {
          this.browsebytime = false;
        } else {
          this.browsebytime = true;
        }
      },
      olderNewer: function(t) {
        if (t === "older") {
          //older
          this.howold = this.howold + 1;
        } else if (t === "newer") {
          //newer
          this.howold = this.howold - 1;
        }
      },
      filterByMonth: function(month) {
        this.searchquery = "";
        this.removeFilters();
        this.isFiltered = true;
        this.filtermonth = month;
      },
      filterNewsItemsByMonth: function(newsitem, filtermonthstring) {
        var newsitempublishMonth = moment(newsitem.publishDate).format("MMM");
        var newsitempublishYear = moment(newsitem.publishDate).format("YYYY");
        if (filtermonthstring == newsitempublishMonth + newsitempublishYear) {
          return true;
        } else {
          return false;
        }
      }
    }
  });
}
//End Checking for the News app

//Check to see if the news app is there
var eventsPageWidget = document.getElementById("events-page-widget");
var todaysDate = moment().format("YYYYMMDD");
if (eventsPageWidget != null) {
  var eventsAPIBaseURL =
    "/api/content/render/false/type/json/query/+contentType:calendarEvent +(conhost:2fd8798a-dffa-45e1-8442-ffac68d5fcf6 conhost:SYSTEM_HOST) +calendarEvent.startDate:[" + todaysDate + " TO 29990101] +languageId:1 +live:true +working:true ";
  var allEventsApiURL =
    eventsAPIBaseURL +
    "+working:true/orderby/calendarEvent.startDate asc/limit/0";
  var featuredEventsApiURL =
    eventsAPIBaseURL +
    "+calendarEvent.featured:1 +working:true/orderby/calendarEvent.startDate asc/limit/1";
  var importantEventsApiURL =
    eventsAPIBaseURL +
    "+calendarEvent.important:1 +working:true/orderby/calendarEvent.startDate asc/limit/2";

  var events = new Vue({
    el: "#events-page-widget",

    data: {
      allevents: null,
      featuredevents: null,
      importantdates: null,
      displayedmonth: null
    },
    computed: {
      displayedevents: function() {
        if (this.displayedmonth != null && this.allevents != null) {
          var filtermonthMonth = moment(this.displayedmonth, "MMM YYYY").format(
            "MMM"
          );
          var filtermonthYear = moment(this.displayedmonth, "MMM YYYY").format(
            "YYYY"
          );
          return this.allevents.filter(event =>
            this.filterEventsByMonth(event, filtermonthMonth + filtermonthYear)
          );
        } else {
          return [];
        }
      },
      eventsLoading: function() {
        if (this.displayedevents != null) {
          return false;
        } else {
          return true;
        }
      }
    },
    created: function() {
      this.getAllEvents();
      this.getFeaturedEvents();
      this.getImportantEvents();
      this.currentMonth();
    },
    filters: {
      formatDate: function(v) {
        //Thursday, April 23rd // 1:00 PM

        return moment(v).format("dddd, MMMM Do, YYYY // h:mm a");
      },
      formatMonth: function(v) {
        return moment(v).format("MMMM YYYY");
      }
    },

    methods: {
      getAllEvents: function() {
        var xhr = new XMLHttpRequest();
        var self = this;
        xhr.open("GET", allEventsApiURL);
        xhr.onload = function() {
          self.allevents = JSON.parse(xhr.responseText).contentlets;
          // console.log(self.allevents.length);
        };
        xhr.send();
      },
      getFeaturedEvents: function() {
        var xhr = new XMLHttpRequest();
        var self = this;
        xhr.open("GET", featuredEventsApiURL);
        xhr.onload = function() {
          self.featuredevents = JSON.parse(xhr.responseText).contentlets;
          // console.log(self.featuredevents.length);
        };
        xhr.send();
      },
      getImportantEvents: function() {
        var xhr = new XMLHttpRequest();
        var self = this;
        xhr.open("GET", importantEventsApiURL);
        xhr.onload = function() {
          self.importantdates = JSON.parse(xhr.responseText).contentlets;
          // console.log(self.importantdates.length);
        };
        xhr.send();
      },
      truncateDescription: function(str) {
        var length = 270;
        var ending = "...";
        if (str.length > length) {
          return str.substring(0, length - ending.length) + ending;
        } else {
          return str;
        }
      },
      currentMonth: function() {
        var date = new Date();
        var y = date.getFullYear();
        var m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        //var lastDay = new Date(y, m + 1, 0);
        this.displayedmonth = firstDay;
      },
      prevMonth: function() {
        this.displayedmonth = moment(this.displayedmonth)
          .subtract(1, "M")
          .toDate();
        var cancelScroll = VueScrollTo.scrollTo("#all-events-container");
        // console.log(this.displayedmonth);
      },
      nextMonth: function() {
        this.displayedmonth = moment(this.displayedmonth)
          .add(1, "M")
          .toDate();
        var cancelScroll = VueScrollTo.scrollTo("#all-events-container");
        // console.log(this.displayedmonth);
      },
      filterEventsByMonth: function(event, filtermonthstring) {
        var eventStartMonth = moment(event.startDate).format("MMM");
        var eventStartYear = moment(event.startDate).format("YYYY");
        if (filtermonthstring == eventStartMonth + eventStartYear) {
          return true;
        } else {
          return false;
        }
      }
    }
  });
}
//End Checking for the Events app

//Look for the Plan a Visit element
var planAVisit = document.getElementById("plan-a-visit");
if( planAVisit != null ) {
      var planAVisitApp = new Vue({el: "#plan-a-visit", data: {showGroupVisitCards: false}});
}
//Done Checking for the Plan a Visit App

//Check if the majors app is there
var majorsPageWidget = document.getElementById("majors-page-widget");
if (majorsPageWidget != null) {
  var majorsapi = "/api/majors/affinitygroups";
  var majors = new Vue({
    el: "#majors-page-widget",

    data: {
      allmajorsdata: null,
      checkedaffinitygroup: [],
      hideFilters: true
    },
    computed: {
      displayedaffinitygroups: function() {
          var tmpaffinitygroups = [];
          if ( this.allmajorsdata == null ) {
            return [];
          } else if( this.checkedaffinitygroup.length != null && this.checkedaffinitygroup.length > 0 && this.allmajorsdata.length > 0 ) {
            for (var i = 0, len = this.checkedaffinitygroup.length; i < len; i++) {
                for (var x = 0, leng = this.allmajorsdata.length; x < leng; x++) {
                    if( this.checkedaffinitygroup[i] === this.allmajorsdata[x].identifier ){
                    tmpaffinitygroups.push( this.allmajorsdata[x] );
                    }
                }
            }
            return sortJsonArray(tmpaffinitygroups, "title", "asc");
          } else {
            return this.allmajorsdata;
          }
      },
      loading: function(){
        return (this.allmajorsdata == null);
      }
    },
    created: function() {
      this.fetchData();
    },
    methods: {
      fetchData: function() {
        var xhr = new XMLHttpRequest();
        var self = this;
        xhr.open("GET", majorsapi);
        xhr.onload = function() {
          self.allmajorsdata = sortJsonArray(JSON.parse(xhr.responseText).results, "title", "asc");
          // console.log(self.allmajorsdata.length);
        };
        xhr.send();
      },
      deselectAll: function() {
          this.checkedaffinitygroup = [];
          document.getElementById("select-all").checked = false;
          document.getElementById("deselect-all").checked = false;
      },
      selectAll: function() {
        if( document.getElementById("select-all").checked ){
            this.checkedaffinitygroup = [];
            for (var i = 0, len = this.allmajorsdata.length; i < len; i++) {
                // Loop through each item in the new array
                this.checkedaffinitygroup.push(this.allmajorsdata[i].identifier);
            }
        }else{
            this.deselectAll();
        }
      },
      splitIntoColumns(data, column, columns) {
        if( data != null){
          const total = data.length; // How many items
          const gap = Math.ceil(total / columns); // How many per col
          let top = (gap * column); // Top of the col
          const bottom = ((top - gap) + 0); // Bottom of the col
          top -= 1; // Adjust top back down one
          return data.filter(item => data.indexOf(item) >= bottom && data.indexOf(item) <= top
          ); // Return the items for the given col
        } else {
          return [];
        }
    },
    showHideFilters: function(){
      this.hideFilters = !this.hideFilters;
    }
    }
  });
}
//Done checking for the majors app

//Check if the video tabs are there
var homeVidoes = document.getElementById("home-videos-tabs");
if (homeVidoes != null) {
    $('#home-videos-tabs li').on('focus', function() {
      var tab = $(this).data('tab');
      $('#home-videos-tabs li').removeClass('is-active');
      $(this).addClass('is-active');
      $('#home-videos-tabs-content div').removeClass('is-active');
      $('div[data-content="' + tab + '"]').addClass('is-active');
    });
}
//Done checking for the majors app

// Enable Vue DevTools
Vue.config.devtools = true;