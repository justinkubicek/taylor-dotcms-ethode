/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 376);
/******/ })
/************************************************************************/
/******/ ({

/***/ 160:
/***/ (function(module, exports) {

var mobileBreakpoint = 768;
var tabletBreakpoint = 1023;
var desktopBreakpoint = 1215;

// function placeImage(screenWidth) { // Change Background Image based on Viewport Width
// 	document.querySelectorAll('[data-interchange]').forEach(function(el) { // Loop through each Data-Interchange element
// 		var imageArrayString = el.dataset.interchange; // Get data stored in data-interchange
// 		var imageArray = imageArrayString.split("]"); // Turn data into an array
// 		for (var i = 0, len = imageArray.length; i < len; i++) { // Loop through each item in the new array
// 			var indexOfComma = imageArray[i].indexOf(',', 5); // Find the index of the comma
// 			if (imageArray[i].search(screenWidth) != -1 ) { // Determine whether or not the current item is for the intended screen width
// 				var imageRoute = imageArray[i].substring(0, indexOfComma); // Grab the route section of the current item
// 				imageRoute = imageRoute.replace('[', ''); // Remove Opening Brackets from route
// 				imageRoute = imageRoute.replace(/\s+/g, ''); // Remove Spaces from route
// 				imageRoute = imageRoute.replace(',', ''); // Remove Commas from route
// 				if ( el.tagName == "DIV" ) { // Determine whether or not the element with the data-interchange attribute is a div
// 					var imageStyles = 'url("' + imageRoute + '")' // Set up style for background image
// 					el.style.backgroundImage = imageStyles; // Add style to the html element
// 				}
// 				else if ( el.tagName == "IMG" ) { // Determine whether or not the element with the data-interchange attribute is an img
// 					el.src = imageRoute; // Add src to html element
// 				}
// 				return;
// 			};
// 		};
// 	});
// }

/* Find Viewport Width and Call Function to Change Background Image */
// function interchangeImage() {
// 	// Note: these breakpoints are currently based on Foundation's Breakpoints, not on Bulma's
// 	var clientWidth = document.documentElement.clientWidth;
// 	if (clientWidth < 640) { // small
// 		placeImage("small");
// 	}
// 	else if (clientWidth < 1024) { // medium
// 		placeImage("medium");
// 	}
// 	else if (clientWidth < 1200) { // large
// 		$('[data-interchange]').each(function() {
// 			placeImage("large");
// 		});
// 	}
// 	else if (clientWidth < 1440) { // xlarge
// 		placeImage("xlarge");
// 	}
// 	else { // xxlarge
// 		placeImage("xxlarge");
// 	}
// }

/* Adjust Page Content Padding based on size of Nav 
/* (necessary because of fixed nav, 
/* and the fact that the fixed nav resizes constantly on mobile 
/* as Taylor University image changes in height) */
// function adjustPageTopPadding() {
// 	if ($(window).width() <= mobileBreakpoint) {
// 		var navBarHeight = document.getElementById('nav-bar-two-logo-large').height();
// 		var navBar
// 		document.getElementById('page-content').css('top',$('.nav-bar-two-logo-large').height() + $('.nav-bar-two-logo-large').position().top);
// 		$('.nav-bar-two-center-content-column-large').css('top',$('.nav-bar-two-logo-large').height() + $('.nav-bar-two-logo-large').position().top - 10);
// 	}
// 	else {
// 		$('.page-content').removeAttr('style');
// 	}
// }

// function clearSearchBox() {
// 	console.log("clear search box");
//    	var x = window.innerWidth
//    	var y = window.innerHeight
//    	if (lastX <= mobileBreakpoint && mobileBreakpoint < x) {
//    		//app.reset();
// 	  	// var searchBox = document.getElementById("search-input-small").vue.reset();
// 	  // 	if (searchBox) {
// 	  // 		console.log(searchBox);
// 			// searchBox.value = "thing";
// 	  // 	}
//    	}
//    	lastX = x
//    	lastY = y
// }

function subsubnavOpen($subsub) {
	// Open the Subsubnav
	$($subsub[1]).removeClass('display-none');
	$($subsub[0]).toggleClass('is-active');
}

function subsubnavClose($subsub) {
	// Close the Subsubnav
	$($subsub[1]).addClass('display-none');
	$($subsub[0]).toggleClass('is-active');
}

// Match size of Subsubnav to size of the element 
// from which it drops down in the Subnav and 
// position the subsubnav under the Subnav element
function subsubnavSize($subsub) {
	$($subsub[1] + ' .visible-menu').css('width', $($subsub[0]).outerWidth());
	var positionOfLink = $($subsub[0]).position();
	$($subsub[1] + ' .visible-menu').css('margin-left', positionOfLink.left);
}

// // Helper function to get an element's exact position
// //https://www.kirupa.com/html5/get_element_position_using_javascript.htm
// function getPosition(el) {
//   var xPos = 0;
//   //var yPos = 0;

//   while (el) {
//     if (el.tagName == "BODY") {
//       // deal with browser quirks with body/window/document and page scroll
//       var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
//       //var yScroll = el.scrollTop || document.documentElement.scrollTop;

//       xPos += (el.offsetLeft - xScroll + el.clientLeft);
//       //yPos += (el.offsetTop - yScroll + el.clientTop);
//     } else {
//       // for all other non-BODY elements
//       xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
//       //yPos += (el.offsetTop - el.scrollTop + el.clientTop);
//     }

//     el = el.offsetParent;
//   }
//   return {
//     x: xPos,
//     //y: yPos
//   };
// }

// function setSearchResultsPosition() {
// 	var searchBar = document.getElementById("nav-large-search-container");
// 	var searchResults = document.getElementById("search-dropdown-container");
// 	if (searchBar && searchResults) {
// 		var searchBarPosition = getPosition(searchBar);
// 		searchResults.style.left = searchBarPosition.x + "px";
// 	}
// }

// window.addEventListener('scroll', function() {
// 	setSearchResultsPosition();
// })

// window.addEventListener('resize', function() {
// 	//interchangeImage();
// 	//adjustPageTopPadding();
// 	//setSearchResultsPosition();
// });

// var lastX = window.innerWidth
// var lastY = window.innerHeight
// window.onresize = clearSearchBox; 


/* -------------------------------------------------------------
BEGIN HIDE HEADER ON SCROLL SCRIPT
------------------------------------------------------------- */
var didScroll = false;
var lastScrollTop = 0;
var delta = 250;
// var navbarHeight = $('.full-nav').outerHeight();

$(window).scroll(function (event) {
	if ($(window).width() > mobileBreakpoint) {
		// make sure user is not on mobile device
		didScroll = true;
	}
});

setInterval(function () {
	if (didScroll) {
		hasScrolled();
		didScroll = false;
	}
}, 250);

function hasScrolled() {
	var st = $(this).scrollTop();

	// Make sure they scroll more than delta
	if (Math.abs(lastScrollTop - st) <= delta) return;

	// If they scrolled down and are past the navbar, add class .nav-up.
	// This is necessary so you never see what is "behind" the navbar.
	if (st > lastScrollTop && st > $('.full-nav').outerHeight()) {
		// Scroll Down
		var outerHeight = -1 * $('.full-nav').outerHeight();
		$('.full-nav').css('top', outerHeight);
		// $('.full-nav').removeClass('nav-down').addClass('nav-up');
	} else {
		// Scroll Up
		if (st + $(window).height() < $(document).height()) {
			$('.full-nav').css('top', 0);
		}
	}

	lastScrollTop = st;
}
/* -------------------------------------------------------------
END HIDE HEADER ON SCROLL SCRIPT
------------------------------------------------------------- */

/* -------------------------------------------------------------
BEGIN /visit PAGE SCRIPT
------------------------------------------------------------- */
// var el1 = document.querySelector("#visit-page-group-visit-button");
// if (el1) {
// 	var visitPageGroupVisitButton = document.querySelector("#visit-page-group-visit-button");
// 	visitPageGroupVisitButton.addEventListener("click", function onclick(event) {
// 	var el2 = document.querySelector("#visit-page-group-visit-cards");
// 	if (el2) {
// 		var visitPageGroupVisitCards = document.querySelector("#visit-page-group-visit-cards");
// 	}
// 	var el3 = document.querySelector("#visit-page-group-visit-button-div");
// 	if (el3) {
// 		var visitPageGroupVisitButtonDiv = document.querySelector("#visit-page-group-visit-button-div");
// 	}
// 	visitPageGroupVisitButtonDiv.style.display = "none";
// 	visitPageGroupVisitCards.style.display = "block";
// });
// }
/* -------------------------------------------------------------
END /visit PAGE SCRIPT
------------------------------------------------------------- */

/* -------------------------------------------------------------
START /scholarships PAGE SCRIPT
------------------------------------------------------------- */
var scholarshipsPanel = document.querySelector("#scholarships-panel");
if (scholarshipsPanel) {}
/* -------------------------------------------------------------
END /scholarships PAGE SCRIPT
------------------------------------------------------------- */

$(document).ready(function () {
	//adjustPageTopPadding();
	//interchangeImage(); // Do First Image Interchange on Document Load
	//setSearchResultsPosition();

	/* -------------------------------------------------------------
 START UNDERLINE MENU SUBNAV SCRIPTS
 ------------------------------------------------------------- */

	// Add svg arrow to elements in subnav and mobile slidein navigation that have submenus
	// $('.menu .has-submenu').append('<div class="svg-submenu-arrow-container svg-submenu-arrow float-right"><svg width="100%" height="100%" viewBox="0 0 100 100" preserveAspectRatio="none"><polyline width="100" height="100" points="0,0 100,50 0,100" stroke="black" fill="none" stroke-width="10"/></div>')
	// $('.hover-underline-menu .has-submenu').append('&nbsp;&nbsp;&nbsp;&nbsp;<div class="svg-submenu-arrow-container svg-submenu-arrow float-right"><svg width="100%" height="100%" viewBox="0 0 100 100" preserveAspectRatio="none"><polyline width="100" height="100" points="0,0 100,50 0,100" stroke="black" fill="none" stroke-width="10"/></div>')

	$(".underline-from-center:not(.has-submenu)").click(function () {
		$('.selected-link').removeClass('selected-link');
	});

	$(".underline-from-center").click(function () {
		$(this).addClass('selected-link');
	});

	/* -------------------------------------------------------------
 END UNDERLINE MENU SUBNAV SCRIPTS
 ------------------------------------------------------------- */

	/* -------------------------------------------------------------
 START OPEN MOBILE SEARCH BAR
 ------------------------------------------------------------- */

	// $('#search-button').click(function() {
	// 	$('.search-bar-small').css('display', 'flex');
	// 	$('.search-bar-small').css('width', '95%');
	// 	$('.search-bar-small').css('margin-left', '2.5%');
	// 	$('.search-bar-small').css('margin-right', '2.5%');
	// 	$('.search-bar-small').find('*').css('display', 'flex');
	// 	$('.close-button-container').css('display', 'initial');
	// 	$('.nav-small .left-container').css('display', 'none');
	// 	$('.nav-small .center-spacer').css('display', 'none');
	// 	$('.nav-small .right-container').css('display', 'none');
	// 	$('.nav-small').css('align-items', 'center');
	// 	$('#search-button').css('display', 'none');
	// 	$('#search-input-small').focus();
	// 	$('#search-close-button-small').css('display', 'flex');
	// 	$('#search-close-button-ul').css('display', 'flex');
	// 	$('#search-close-button-ul').find('*').css('display', 'flex');

	// });

	// // document.getElementById("search-close-button-small").addEventListener("click", function() {
	// //     document.getElementById("search-input-small").value = "";
	// // });

	// $('#search-close-button-small').click(function() {
	// 	$('.search-dropdown-container').css('display', 'none');
	// 	$('#search-close-button-small').css('display', 'none');
	// 	$('.search-bar-small').css('display', 'none');
	// 	$('.search-bar-small').css('width', '0');
	// 	$('.search-bar-small').css('margin-left', '0');
	// 	$('.search-bar-small').css('margin-right', '0');
	// 	$('.search-bar-small').find('*').css('display', 'none');
	// 	$('.close-button-container').css('display', 'none');
	// 	$('.nav-small .left-container').css('display', 'flex');
	// 	$('.nav-small .center-spacer').css('display', 'flex');
	// 	$('.nav-small .right-container').css('display', 'flex');
	// 	$('.nav-small').css('align-items', 'initial');
	// 	$('#search-button').css('display', 'initial');
	// });

	// $(".hamburger-menu-container").click(function() {
	//     $(".top-bar-small-container").toggleClass("top-bar-small-container-change");
	// });

	$("#off-canvas-nav-close-button").click(function () {
		$(".top-bar-small-container").removeClass("top-bar-small-container-change");
	});

	/* -------------------------------------------------------------
 END OPEN MOBILE SEARCH BAR
 ------------------------------------------------------------- */

	/* -------------------------------------------------------------
 START SUBSUBNAV SCRIPTS
 ------------------------------------------------------------- */

	$('.has-submenu').click(function () {
		// Add class of active to links with submenus
		$(this).toggleClass('is-active');
	});

	// Store all subsubnav id information
	var $subsubnavs = [["#graduate-link", "#graduate-subnav-menu"], ["#how-to-apply-link", "#how-to-apply-subnav-menu"], ["#study-abroad-link", "#study-abroad-subnav-menu"], ["#high-school-programs-link", "#high-school-programs-subnav-menu"], ["#chapel-link", "#chapel-subnav-menu"], ["#visit-link", "#visit-subnav-menu"]];

	for (var i = 0; i < $subsubnavs.length; i++) {
		// Loop through all subsubnavs and set size for each
		if ($($subsubnavs[i][0]).length) {
			// Check if subsubnav link exists on current page
			subsubnavSize($subsubnavs[i]);
		}
	}

	// GRADUATE SUBSUBNAV
	$('#graduate-link, #graduate-subnav-menu .visible-menu').hover(function () {
		subsubnavOpen($subsubnavs[0]);
	}, function () {
		subsubnavClose($subsubnavs[0]);
	});

	// HOW TO APPLY SUBSUBNAV
	// $('#how-to-apply-link, #how-to-apply-subnav-menu .visible-menu').hover(
	// 	function() {
	// 		subsubnavOpen($subsubnavs[1]);
	// 	},
	// 	function() {
	// 		subsubnavClose($subsubnavs[1]);
	// 	}
	// );

	// STUDY ABROAD SUBSUBNAV
	$('#study-abroad-link, #study-abroad-subnav-menu .visible-menu').hover(function () {
		subsubnavOpen($subsubnavs[2]);
	}, function () {
		subsubnavClose($subsubnavs[2]);
	});

	// HIGH SCHOOL PROGRAMS SUBSUBNAV
	$('#high-school-programs-link, #high-school-programs-subnav-menu .visible-menu').hover(function () {
		subsubnavOpen($subsubnavs[3]);
	}, function () {
		subsubnavClose($subsubnavs[3]);
	});

	// CHAPEL SUBSUBNAV
	// var chapelNavItem = document.getElementById("chapel-link");
	// var chapelSubsub = document.getElementById('chapel-subnav-menu');
	// var chapelSubsubVisibleMenu = chapelSubsub.childNodes[0].childNodes[0];
	// chapelNavItem.addEventListener("mouseenter", function() {
	// 	chapelSubsub.classList.remove("display-none");
	// });
	// chapelSubsubVisibleMenu.addEventListener("mouseenter", function() {
	// 	chapelSubsub.classList.remove("display-none");
	// });
	// chapelNavItem.addEventListener("mouseleave", function() {
	// 	chapelSubsub.classList.add("display-none");
	// });
	// chapelSubsubVisibleMenu.addEventListener("mouseleave", function() {
	// 	chapelSubsub.classList.add("display-none");
	// });
	// console.log(chapelSubsubVisibleMenu);

	// $('#chapel-link, #chapel-subnav-menu .visible-menu').hover(
	// 	function() {
	// 		subsubnavOpen($subsubnavs[4]);
	// 	},
	// 	function() {
	// 		subsubnavClose($subsubnavs[4]);
	// 	}
	// );

	// VISIT SUBSUBNAV
	$('#visit-link, #visit-subnav-menu .visible-menu').hover(function () {
		subsubnavOpen($subsubnavs[5]);
	}, function () {
		subsubnavClose($subsubnavs[5]);
	});

	/* -------------------------------------------------------------
 END SUBSUBNAV SCRIPTS
 ------------------------------------------------------------- */

	/* -------------------------------------------------------------
 START JQUERY SMOOTH SCROLLING TO LINK ON SAME PAGE SCRIPT
 ------------------------------------------------------------- */
	//css-tricks.com/snippets/jquery/smooth-scrolling/ (modified)
	$('a[href*="#"]') // Select all links with hashes
	// Remove links that don't actually link to anything
	.not('[href="#"]').not('[href="#0"]').click(function (event) {

		// Change offset of page scroll ending point based on nav size
		var navOffset = 0;
		if ($(window).width() < 640) {
			navOffset = 200;
		} else if ($(window).width() < 1008) {
			navOffset = 200;
		} else {
			navOffset = 230;
		}

		// On-page links
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			// Figure out element to scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top - navOffset
				}, 500);
			}
		}
	});
	/* -------------------------------------------------------------
 END JQUERY SMOOTH SCROLLING TO LINK ON SAME PAGE SCRIPT
 ------------------------------------------------------------- */

	/*------------------------------------------------------------
 START SLIDEOUT.JS SCRIPT 
 -------------------------------------------------------------*/
	var slideout = new Slideout({
		'panel': document.getElementById('panel'),
		'menu': document.getElementById('menu'),
		'side': 'right'
	});

	$('.slideout-toggle').click(function () {
		slideout.toggle();
	});
	/*------------------------------------------------------------
 END SLIDEOUT.JS SCRIPT 
 -------------------------------------------------------------*/
}); // END DOCUMENT READY FUNCTION

/***/ }),

/***/ 376:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(160);


/***/ })

/******/ });