package com.ethode.algolia.search.classes;

public class Page {

    private String page_title;
    private String page_meta_description;
    private String category;
    private String page_uri;

  
    public Page() {}

    //page_title
  
    public String getPageTitle() {
      return page_title;
    }
  
    public Page setPageTitle( String page_title ) {
      this.page_title = page_title;
      return this;
    }

    //page_meta_description

    public String getPage_Meta_Description() {
      return page_meta_description;
    }
  
    public Page setPage_Meta_Description( String page_meta_description ) {
      this.page_meta_description = page_meta_description;
      return this;
    }

    //category

    public String getCategory() {
      return category;
    }
  
    public Page setCategory( String category ) {
      this.category = category;
      return this;
    }


    //page_uri

    public String getPage_Uri() {
      return page_uri;
    }

    public Page setPage_Uri( String page_uri ) {
      this.page_uri = page_uri;
      return this;
    }

  }